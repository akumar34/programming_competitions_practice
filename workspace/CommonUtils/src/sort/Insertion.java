package sort;

import java.util.List;

public class Insertion{
	public static <AnyType extends Comparable<? super AnyType>> void sort(List<AnyType> array){
		int n = array.size();
		for(int i = 0; i < n; i++){
			int j = i + 1;
			if(j >= n) j = n-1;
			while(j >= 1 && SortUtils.less(array.get(j), array.get(j-1))){
				SortUtils.exch(array, j, j-1);
				j--;
			}
		}
	}
}
