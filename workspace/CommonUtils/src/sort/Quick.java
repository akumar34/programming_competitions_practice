package sort;

import java.util.List;

import commonutils.BasicAlgorithms;

public class Quick{

	public static <AnyType extends Comparable<? super AnyType>> void sort(List<AnyType> array, int left, int right){
		if(right <= (left + 10 - 1)){
			Insertion.sort(array);
			return;
		}
		int pivot = (int)BasicAlgorithms.select(array, left + (right-left)/2, left, right);
		sort(array, left, pivot-1);
		sort(array, pivot+1, right);
	}
}
