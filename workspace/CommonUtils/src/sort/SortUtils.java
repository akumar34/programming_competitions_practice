package sort;

import java.util.List;

public class SortUtils {

	public static <AnyType extends Comparable<? super AnyType>> boolean less(AnyType a, AnyType b){ 
		return a.compareTo(b) < 0; 
	}
	
	/*public static void exch(Object[] a, int i, int j){
		Object swap = a[i];
		a[i] = a[j];
		a[j] = swap;
	}*/
	
	public static <AnyType> void exch(List<AnyType> a, int i, int j){
		AnyType temp = a.get(i);
		a.set(i, a.get(j));
		a.set(j, temp);
	}
	
	public static <AnyType> void print(List<AnyType> a){ 
		for(int i = 0; i < a.size(); i++) System.out.println(a.get(i));
	}
	
	/*@SuppressWarnings("rawtypes")
	public static void show(Comparable[] a){ 
		for(int i = 0; i < a.length; i++) System.out.println(a[i]);
	}*/
}
