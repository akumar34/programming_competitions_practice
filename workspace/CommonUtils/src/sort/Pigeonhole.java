package sort;

import java.util.ArrayList;
import java.util.List;

import containers.Pair;

public class Pigeonhole {
	public static void sort(Integer[] array){
		int max = array[0];
		for(int i = 0; i < array.length; i++) 
			if(SortUtils.less(max, array[i])) 
				max = array[i];
		
		Integer[] newArray = new Integer[max + 1];
		
		for(int i = 0; i < array.length; i++) newArray[array[i]]++;
		
		int j = 0;
		int i = 0;
		while(j < newArray.length){
			int value = newArray[j];
			while(newArray[j] != 0){
				array[i] = value;
				i++;
				newArray[j]--;
			}
			j++;
		}
	}
	@SuppressWarnings("rawtypes")
	public static void sort(List<Pair> array){
		int max = (int)array.get(0).getSecond();
		for(int i = 0; i < array.size(); i++) 
			if(SortUtils.less(max, (int)array.get(i).getSecond())) 
				max = (int)array.get(i).getSecond();

		List<List<Pair>> newArray = new ArrayList<List<Pair>>(max);
		for(int i = 0; i < max + 1; i++) newArray.add(null);
		
		for(int i = 0; i < array.size(); i++){ 
			int newArrayIndex = (int)(array.get(i).getSecond());
			List<Pair> element = newArray.get(newArrayIndex);
			if(element == null) element = new ArrayList<Pair>();
			element.add(array.get(i));
			newArray.set(newArrayIndex, element);
		}
		
		int j = 0;
		int i = 0;
		while(j < newArray.size()){
			List<Pair> value = newArray.get(j);
			if(value == null){
				j++;
				continue;
			}
			while(! value.isEmpty()){
				array.set(i, value.get(0));
				value.remove(0);
				i++;
			}
			j++;
		}
	}
}
