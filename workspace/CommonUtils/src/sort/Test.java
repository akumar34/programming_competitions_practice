package sort;

import java.util.ArrayList;
import java.util.List;

public class Test {
	public static void main(String [] args){
		List<Double> data = new ArrayList<Double>();
		data.add(1.0);
		data.add(3.2);
		data.add(2.9);
		data.add(3.1);
		data.add(2.5);
		
		Insertion.sort(data);
		
		for(Double item : data){
			System.out.println(item);
		}
	}
}
