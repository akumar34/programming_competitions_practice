package commonutils;

import java.util.ArrayList;
import java.util.List;

import sort.SortUtils;

public class BasicAlgorithms {

	public static <AnyType extends Comparable<? super AnyType>> int partition(List<AnyType> array, int left, int right){
		int i = left; 
		int j = right+1;
		while(i<=j){
			while(SortUtils.less(array.get(++i), array.get(left))) if(i == right) break;
			while(SortUtils.less(array.get(left), array.get(--j))) if(j == left) break;
			if(i >= j) break;
			SortUtils.exch(array, i, j);
		}
		SortUtils.exch(array, left, j);
		return j;
	}
	/*@SuppressWarnings("rawtypes")
	public static <AnyType extends Comparable<? super AnyType>> int medianOfMedians(List<AnyType> array, int k){
		int n = array.size();
		if(n <= 5){
			Insertion.sort(array);
			return k;
		}
		Comparable[][] arrays = CommonUtils.splitArray(array, 5);
		for(int i = 0; i < arrays.length; i++) Insertion.sort(arrays[i]);
		Comparable[] medianArray = new Comparable[arrays.length];
		for(int i = 0; i < medianArray.length; i++) medianArray[i] = arrays[i][2];
		return medianOfMedians(medianArray, n/10);
	}*/
	
	public static <AnyType extends Comparable<? super AnyType>> int medianOf3(List<AnyType> array, int left, int right){
		int mid = (left + right)/2;
		if(SortUtils.less(array.get(right),array.get(left))) SortUtils.exch(array, right, left);
		if(SortUtils.less(array.get(mid), array.get(left))) SortUtils.exch(array, mid, left);
		if(SortUtils.less(array.get(right), array.get(mid))) SortUtils.exch(array, right, mid);
		return mid;
	}
	
	//select kth smallest element
	public static <AnyType extends Comparable<? super AnyType>> AnyType select(List<AnyType> array, int k, int left, int right){
		int pivot = medianOf3(array, left, right);
		SortUtils.exch(array, pivot, left);
		pivot = partition(array,left,right);
		if(k == pivot) return array.get(k);
		if(k < pivot) return select(array,k,left,pivot-1);
		return select(array,k,pivot+1,right);
	}

	//size is total bits
	public static List<Integer[]> generateBinaryNumbers(int size){
		return binaryNumbers(new Integer[size], 0, size, new ArrayList<Integer[]>());
	}
	
	public static List<Integer[]> binaryNumbers(Integer[] array, int i, int N, List<Integer[]> list){
		if(i == N){
			Integer[] temp = new Integer[N];
			for(int j = 0; j < temp.length; j++) temp[j] = array[j];
			list.add(temp);
			return list;
		}
		array[i] = 0;
		binaryNumbers(array, i+1, N, list);
		array[i] = 1;
		binaryNumbers(array, i+1, N, list);
		return list;
	}
	
	public static List<Integer[]> generateSubsets(Integer[] input){
		return subsets(new Integer[input.length], 0, input.length, new ArrayList<Integer[]>(), input);
	}
	
	public static List<Integer[]> subsets(Integer[] array, int i, int N, List<Integer[]> list, Integer[] input){
		if(i == N){
			List<Integer> subset = new ArrayList<Integer>();
			for(int j = 0; j < N; j++) if(array[j] == 1) subset.add(input[j]);
			Integer[] subsetArray = (Integer[])subset.toArray(new Integer[subset.size()]);
			list.add(subsetArray);
			return list;
		}
		array[i] = 0;
		subsets(array, i+1, N, list, input);
		array[i] = 1;
		subsets(array, i+1, N, list, input);
		return list;
	}
	
	public static List<Integer[]> permutations(Integer[] array){
		if(array.length == 1){
			List<Integer[]> list = new ArrayList<Integer[]>();
			Integer[] items = new Integer[1];
			items[0] = array[0];
			list.add(items);
			return list;
		}
		if(array.length == 2){
			return CommonUtils.permutationOf2Items(array[0], array[1]);
		}
		
		Integer[] first = {array[0]};
		
		Integer[] remaining = new Integer[array.length-1];
		for(int i = 1,j=0; i < array.length; i++,j++)
			remaining[j] = array[i];
		
		List<Integer[]> list1 = permutations(first);
		List<Integer[]> list2 = permutations(remaining);
		
		List<Integer[]> list = CommonUtils.mergePermutations(list1, list2);
		return list;
	}
}
