package commonutils;

import containers.Pair;

public class AdvancedAlgorithms {
	/*
	 * Coin Change problem: 
	 * Find the minimum number of coins needed in change. Ex: 3 dimes needed for 30 cents in change when
	 * the denominations are: 1 cent, 10 cents, 25 cents.
	 * 
	 * Note that if the denominations are: 1,5,10,25,50, then we can use greedy instead of dynamic programming.
	 * 
	 * OPT(0) = 0
	 * OPT(j) = {
	 * 				infinite if j < 0,
	 * 				0 if j = 0,
	 * 				Otherwise choose the denomination d[i] that maximizes OPT[j-d[i]] + 1
	 * 			}
	 */
	public static Pair<Integer,Integer[]> coinChange(Integer[] denominations, int change){
		Integer[] c = new Integer[change + 1];
		Integer[] pathMap = new Integer[change + 1];
		int noOfCoins = coinChange(c,pathMap,denominations, change);
		return new Pair<Integer,Integer[]>(noOfCoins,pathMap);
	}
	public static int coinChange(Integer[] c, Integer[] pathMap, Integer[] denominations, int change){
		c[0] = 0;
		for(int amount = 1; amount <= change; amount++){
			c[amount] = Integer.MAX_VALUE;
			for(int denomination : denominations){
				if(amount < denomination) continue;
				int totalCoins = 1 + c[amount-denomination];
				if(totalCoins >= c[amount]) continue;
				c[amount] = totalCoins;
				pathMap[amount] = denomination;
			}
		}
		return c[change];
	}
	
	public static void printCoinChange(Integer[] pathMap, int change){
		if(change > 0){
			printCoinChange(pathMap, change - pathMap[change]);
			System.out.print(pathMap[change] + " ");
		}
	}
	
	/*
	 * Edit Distance problem: 
	 * Find the minimum number of inserts, deletes, and substitutions to transform string1 to string2.
	 * 
	 * OPT(0,0) = 0
	 * OPT(0,i) = i
	 * OPT(j,0) = j
	 * OPT(i,j) = {
	 * 				min(
	 * 					OPT(i,j-1) + 1,
	 * 					OPT(i-1,j) + 1,
	 * 					OPT(i-1,j-1) + S such that S = 0 if string1[i] == string2[j] or S = 1 if string1[i] != string2[j]
	 * 				) 
	 * 			}
	 */
	public static Pair<Integer,String[][]> editDistance(char[] string1, char[] string2){
		Integer[][] distance = new Integer[string1.length + 1][string2.length + 1];
		String[][] pathMap = new String[string1.length + 1][string2.length + 1];
		int editDistance = editDistance(distance, pathMap, string1, string2);
		return new Pair<Integer, String[][]>(editDistance, pathMap);
	}
	public static int editDistance(Integer distance[][], String pathMap[][], char[] string1, char[] string2){
		int str1Length = string1.length;
		int str2Length = string2.length;
		
		distance[0][0] = 0;
		pathMap[0][0] = "SUBSTITUTE";
		
		for(int i = 1; i <= str1Length; i++){
			distance[i][0] = new Integer(i);
			pathMap[i][0] = "INSERT";
		}
		for(int j = 1; j <= str2Length; j++){
			distance[0][j] = new Integer(j);
			pathMap[0][j] = "DELETE";
		}
		for(int i = 1; i <= str1Length; i++){
			for(int j = 1; j <= str2Length; j++){
				int subCost = 0;
				if(string1[i-1] != string2[j-1]) subCost = 1;
				distance[i][j] = distance[i-1][j]+1;
				pathMap[i][j] = "DELETE";
				if(distance[i][j-1] + 1 < distance[i][j]){
					distance[i][j] = distance[i][j-1] + 1;
					pathMap[i][j] = "INSERT";
				}
				if(distance[i-1][j-1] + subCost <= distance[i][j]){
					distance[i][j] = distance[i-1][j-1] + subCost;
					pathMap[i][j] = "SUBSTITUTE";
				}
			}
		}
		return distance[str1Length][str2Length];
	}
	
	public static void printEditDistance(String pathMap[][], char[] string1, char[] string2){
		printEditDistance(pathMap, string1, string2, string1.length,string2.length);
	}
	public static void printEditDistance(String pathMap[][],char[] string1, char[] string2, int i, int j){
		if(i == 0 || j == 0) return;
		if(pathMap[i][j].equals("SUBSTITUTE")){
			printEditDistance(pathMap, string1, string2, i-1, j-1);
			System.out.println("substitute " + string1[i-1] + " with " + string2[j-1]);
		}
		else if(pathMap[i][j].equals("DELETE")){
			printEditDistance(pathMap, string1, string2, i-1, j);
			System.out.println("delete " + string1[i-1]);
		}
		else{
			printEditDistance(pathMap, string1, string2, i, j-1);
			System.out.println("insert " + string2[j-1]);
		}
	}
}
