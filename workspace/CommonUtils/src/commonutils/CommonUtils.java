package commonutils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import sort.SortUtils;

public class CommonUtils {
	public static int[] stringArrayToIntegerArray(String[] strs){
		List<Integer> list = new ArrayList<Integer>();
		for(String item : strs) list.add(Integer.valueOf(item));
		int[] ints = new int[list.size()];
		for(int i = 0; i < ints.length; i++) ints[i] = list.get(i);
		return ints;
	}
	public static Integer[] intArrayToIntegerArray(int[] ints){
		Integer[] array = new Integer[ints.length];
		for(int item = 0; item < ints.length; item++) array[item] = ints[item];
		return array;
	}
	public static List<Integer> intArrayToList(int[] ints){
		List<Integer> list = new ArrayList<Integer>();
		for(int i = 0; i < ints.length; i++) list.add(ints[i]);
		return list;
	}
	public static BigDecimal ratio(int numerator, int denominator){
		if(denominator == 0) return BigDecimal.ZERO;
		BigDecimal num = (new BigDecimal(numerator));
		BigDecimal den = (new BigDecimal(denominator));
		num.setScale(15);
		den.setScale(15); 
		return ((num.divide(den,15,BigDecimal.ROUND_HALF_UP)));
	}
	public static BigDecimal ratio(String numerator, String denominator){
		int intNumerator = Integer.valueOf(numerator);
		int intDenominator = Integer.valueOf(denominator);
		if(intDenominator == 0) return BigDecimal.ZERO;
		BigDecimal num = (new BigDecimal(intNumerator));
		BigDecimal den = (new BigDecimal(intDenominator));
		num.setScale(15);
		den.setScale(15); 
		return ((num.divide(den,15,BigDecimal.ROUND_HALF_UP)));
	}
	public static void swap(int array[], int source, int target){
		int temp = array[source];
		array[source] = array[target];
		array[target] = temp;
	}
	@SuppressWarnings("rawtypes")
	public static Comparable[][] splitArray(Comparable[] array, int sizePerArray){
		int noOfArrays = array.length/sizePerArray;
		Comparable[][] arrays = new Comparable[noOfArrays][];
		int left = 0;
		for(int arrayNo = 0; arrayNo < noOfArrays; arrayNo++){
			Comparable[] newArray = new Comparable[sizePerArray];
			for(int i = 0; i <  sizePerArray; i++){
				newArray[i] = array[left];
				left++;
			}
			arrays[arrayNo] = newArray;
		}
		return arrays;
	}
	public static int absoluteDifference(int operand1, int operand2){
		return (new BigDecimal(operand1).subtract(new BigDecimal(operand2))).abs().intValue();
	}

	public static <AnyType extends Comparable<? super AnyType>> AnyType min(AnyType a, AnyType b){
		if(SortUtils.less(a, b)) return a;
		return b;
	}

	public static <AnyType extends Comparable<? super AnyType>> AnyType max(AnyType a, AnyType b){
		if(SortUtils.less(b, a)) return a;
		return b;
	}
	
	public static List<Integer[]> permutationOf2Items(int item1, int item2){
		List<Integer[]> permutations = new ArrayList<Integer[]>();
		Integer[] items1 = {item1, item2};
		Integer[] items2 = {item2, item1};
		permutations.add(items1);
		permutations.add(items2);
		return permutations;
	}
	
	public static List<Integer[]> mergePermutations(List<Integer[]> list1, List<Integer[]> list2){
		List<Integer[]> merge = new ArrayList<Integer[]>();
		for(Integer[] items1 : list1){
			for(Integer[] items2 : list2){
				int insertIndex = 0;
				for(int insertNo = 0; insertNo < items2.length + 1; insertNo++){
					Integer[] newItem = new Integer[items2.length + 1];
					int j = 0;
					for(int i = 0; i < newItem.length; i++){
						if(i == insertIndex) continue;
						newItem[i] = items2[j];
						j++;
					}
					newItem[insertIndex] = items1[0];
					insertIndex++;
					merge.add(newItem);
				}
			}
		}
		return merge;
	}
}
