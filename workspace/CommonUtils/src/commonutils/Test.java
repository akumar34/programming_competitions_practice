package commonutils;

import java.util.List;

import containers.Pair;

public class Test {
	public static void main(String [] args){
		List<Integer[]> list = BasicAlgorithms.generateBinaryNumbers(9);
		for(Integer[] array : list){
			for(Integer item : array) System.out.print(item);
			System.out.println();
		}
		
		System.out.println();
		
		Integer[] input = {9,8,3,1};
		list = BasicAlgorithms.generateSubsets(input);
		for(Integer[] array : list){
			for(Integer item : array) System.out.print(item);
			System.out.println();
		}
		
		System.out.println();
		
		Integer[] input2 = {1,2,3,4};
		list = BasicAlgorithms.permutations(input2);
		for(Integer[] array : list){
			for(Integer item : array) System.out.print(item);
			System.out.println();
		}
		
		System.out.println();
		
		Integer[] denominations = {1,10,25};
		int change = 30;

		Pair<Integer,Integer[]> coinChange = AdvancedAlgorithms.coinChange(denominations, change);
		System.out.println(coinChange.getFirst());
		
		AdvancedAlgorithms.printCoinChange((Integer[])coinChange.getSecond(), change);
		
		System.out.println();
		
		Pair<Integer,String[][]> editDistance = AdvancedAlgorithms.editDistance("cats".toCharArray(), "network".toCharArray());
		System.out.println(editDistance.getFirst());
		
		AdvancedAlgorithms.printEditDistance((String[][])editDistance.getSecond(), "cats".toCharArray(), "network".toCharArray());
		
		System.out.println();
		
		System.out.println("total: " + MathUtils.sum(9, 23));
		
		System.out.println("total: " + MathUtils.sum(Integer.MAX_VALUE, 23));
		
		
	}
}
