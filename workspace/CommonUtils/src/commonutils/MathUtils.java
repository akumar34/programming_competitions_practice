package commonutils;

public class MathUtils {
	public static int sum(int a, int b){
		try{
		if(a < 0 && b < 0 && (a + b) > 0) throw new Exception();
		if(a > 0 && b > 0 && (a + b) < 0) throw new Exception();
		} catch(Exception e){
			e.printStackTrace();
			System.out.println("Error: Overflow.");
		}
		return a + b;
	}
}
