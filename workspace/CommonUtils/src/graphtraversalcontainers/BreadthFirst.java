package graphtraversalcontainers;

import java.util.HashMap;
import java.util.List;

import containers.Queue;
import graphcontainers.Graph;

public class BreadthFirst<AnyType extends Comparable<AnyType>> extends GraphTraversal<AnyType> {

	public BreadthFirst(Graph<AnyType> graph) {
		super(graph);
	}

	@Override
	public HashMap<Integer,Boolean> search(int source) {
		Queue<Integer> queue = new Queue<Integer>();
		queue.push(source);
		discovered.put(source, Boolean.TRUE);
		while(! queue.isEmpty()){
			int vertex = queue.pop();
			processVertex(vertex);
			processed.put(vertex, Boolean.TRUE);
			List<Integer> neighbors = graph.neighbors(vertex);
			for(int neighbor : neighbors){
				if(discovered.get(neighbor) == Boolean.FALSE){
					queue.push(neighbor);
					discovered.put(neighbor, Boolean.TRUE);
					pathMap.put(neighbor, vertex);
				}
				if(processed.get(neighbor) == Boolean.FALSE)
					processEdge(vertex, neighbor);
			}
		}
		return discovered;
	}

	@Override
	protected void processVertex(int vertex) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void processEdge(int source, int target) {
		// TODO Auto-generated method stub
		
	}
}
