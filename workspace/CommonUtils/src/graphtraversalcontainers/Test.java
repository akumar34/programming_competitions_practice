package graphtraversalcontainers;

import graphcontainers.AdjList;
import graphcontainers.Graph;

import java.util.List;

public class Test {
	public static void main(String [] args){
		Graph<Integer> graph1 = new AdjList<Integer>();
		graph1.addEdge(1,3);
		graph1.addEdge(3,1);
		graph1.addEdge(1,2);
		graph1.addEdge(2,1);
		graph1.addEdge(3,2);
		graph1.addEdge(2,3);
		graph1.print(); 
		System.out.println();
		DepthFirst<Integer> dfs1 = new DepthFirst<Integer>(graph1);
		dfs1.search(1);
		System.out.println("Is there a cycle? " + dfs1.isCycle());
		
		graph1 = new AdjList<Integer>();
		graph1.addEdge(1,3);
		graph1.addEdge(3,1);
		graph1.addEdge(1,2);
		graph1.addEdge(2,1);
		graph1.print(); 
		System.out.println();
		dfs1 = new DepthFirst<Integer>(graph1);
		dfs1.search(1);
		System.out.println("Is there a cycle? " + dfs1.isCycle());
		
		graph1 = new AdjList<Integer>();
		graph1.addEdge(1,3);
		graph1.addEdge(1,2);
		graph1.addEdge(3,2);
		graph1.print(); 
		System.out.println();
		dfs1 = new DepthFirst<Integer>(graph1);
		dfs1.search(1);
		System.out.println("Is there a cycle? " + dfs1.isCycle());
		
		Graph<Integer> graph = new AdjList<Integer>();
		
		graph.addEdge(1,2);
		graph.addEdge(2,3);
		graph.addEdge(3,4);
		
		
		List<Integer> list1 = GraphTraversalUtils.path(1, 4, ShortestPathUtils.unweightedShortestPath(graph, 1));
		for(Integer item : list1){
			System.out.println(item);
		}

		System.out.println();
		
		graph.addEdge(4,1);
		GraphTraversal<Integer> dfs = new DepthFirst<Integer>(graph);
		
		dfs.search(1);
		for(Integer item : GraphTraversalUtils.path(1, 4, dfs.pathMap())){
			System.out.println(item);
		}		
		
		System.out.println();
		
		graph.addEdge(9,22);
		System.out.println("total components: " + GraphTraversalUtils.totalConnectedComponents(graph));
		
		graph.removeEdge(4, 1);
		
		System.out.println();
		TopologicalSort<Integer> ts = new TopologicalSort<Integer>(graph);
		List<Integer> result = ts.sort();
		
		for(Integer item : result)
			System.out.println("TS: " + item);
		
		AdjList<Integer> spGraph = new AdjList<Integer>();
		
		spGraph.addEdge(1,3,2);
		spGraph.addEdge(3,1,2);
		
		spGraph.addEdge(1,2,4);
		spGraph.addEdge(2,1,4);
		
		spGraph.addEdge(2,4,5);
		spGraph.addEdge(4,2,5);
		
		spGraph.addEdge(2,3,1);
		spGraph.addEdge(3,2,1);
		
		spGraph.addEdge(3,4,8);
		spGraph.addEdge(4,3,8);
		
		spGraph.addEdge(3,5,10);
		spGraph.addEdge(5,3,10);
		
		spGraph.addEdge(4,5,2);
		spGraph.addEdge(5,4,2);
		
		spGraph.addEdge(4,6,6);
		spGraph.addEdge(6,4,6);
		
		spGraph.addEdge(5,6,3);
		spGraph.addEdge(6,5,3);
		

		List<Integer> list2 = GraphTraversalUtils.path(1, 6, ShortestPathUtils.nonNegWeightedShortestPath(spGraph, 1));
		for(int vertex : list2){
			System.out.println(vertex);
		}
		
		System.out.println();
		
		Graph<Integer> graph4 = new AdjList<Integer>();
		graph4.addEdge(5,2);
		graph4.addEdge(5,0);
		graph4.addEdge(4,0);
		graph4.addEdge(4,1);
		graph4.addEdge(2,3);
		graph4.addEdge(3,1);
		
		TopologicalSort<Integer> ts1 = new TopologicalSort<Integer>(graph4);
		List<Integer> list3 = ts1.sort();
		for(int vertex : list3){
			System.out.println(vertex);
		}
		
		System.out.println();
		
		List<Integer> ts2 = GraphTraversalUtils.topologicalSort(graph4);
		for(int vertex : ts2)
			System.out.println(vertex);
		
		System.out.println();
		
		AdjList<Integer> graph5 = new AdjList<Integer>();
		graph5.addEdge(1,2,5);
		graph5.addEdge(1,4,-2);
		graph5.addEdge(2,3,1);
		graph5.addEdge(3,4,2);
		graph5.addEdge(3,5,7);
		graph5.addEdge(3,6,3);
		graph5.addEdge(4,2,2);
		graph5.addEdge(4,5,3);
		graph5.addEdge(5,6,10);
		
		List<Integer> list4 = GraphTraversalUtils.path(1, 6, ShortestPathUtils.negativeWeightedShortestPath(graph5, 1));
		for(int vertex : list4){
			System.out.println(vertex);
		}		
		
	}
}