package graphtraversalcontainers;

import java.util.HashMap;

import graphcontainers.GraphEdge;
import graphcontainers.Graph;

public class DepthFirst<AnyType extends Comparable<AnyType>> extends GraphTraversal<AnyType>{
	private boolean isCycle;
	private GraphEdge<AnyType> cycleEdge = null;
	
	public DepthFirst(Graph<AnyType> graph) { 
		super(graph); 
		isCycle = false;
	}
	
	@Override
	public HashMap<Integer,Boolean> search(int source) {
		isCycle = false;
		return postOrderTraversal(source);
	}

	public HashMap<Integer,Boolean> search() {
		isCycle = false;
		HashMap<Integer,Boolean> traversed = new HashMap<Integer, Boolean>();
		
		for(int source : graph.vertices()){
			if(traversed.get(source) == Boolean.TRUE) continue;
			traversed = postOrderTraversal(source);
		}
		
		return traversed;
	}
	
	protected HashMap<Integer,Boolean> postOrderTraversal(int source){
		discovered.put(source, Boolean.TRUE);
		for(int neighbor : graph.neighbors(source)){
			if(discovered.get(neighbor) == Boolean.FALSE){
				pathMap.put(neighbor, source);
				postOrderTraversal(neighbor);
			}
			else if(processed.get(neighbor) == Boolean.FALSE) 
				processEdge(source, neighbor);
		}
		processed.put(source, Boolean.TRUE);
		processVertex(source);
		return discovered;
	}

	@Override
	protected void processVertex(int vertex) {
		// TODO Auto-generated method stub
	}

	@Override
	protected void processEdge(int source, int target) {
		if(cycleEdge != null) return;
		if(pathMap.get(source) == target) return; 
		isCycle = true;
		cycleEdge = new GraphEdge<AnyType>(source, target);
	}
	
	public boolean isCycle(){
		return isCycle;
	}
	
	public GraphEdge<AnyType> cycleEdge(AnyType source){
		return cycleEdge;
	}
}
