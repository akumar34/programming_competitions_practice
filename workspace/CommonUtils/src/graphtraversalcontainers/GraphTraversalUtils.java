package graphtraversalcontainers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import networkflowcontainers.NetworkEdge;
import networkflowcontainers.NetworkGraph;
import containers.Heap;
import containers.Queue;
import graphcontainers.AdjList;
import graphcontainers.Edge;
import graphcontainers.GraphEdge;
import graphcontainers.Graph;
import graphcontainers.GraphUtils;
import graphcontainers.Vertex;
import unionfindcontainers.UnionFind;

public class GraphTraversalUtils {
	@SuppressWarnings("unchecked")
	public static <AnyType extends Comparable<AnyType>> boolean  isCycle(Graph<AnyType> graph){
		UnionFind<AnyType> disjointSets = new UnionFind<AnyType>();
		List<GraphEdge<AnyType>> edges = (List<GraphEdge<AnyType>>)graph.edges();
		for(Edge<AnyType> edge : edges){
			AnyType source = disjointSets.find(edge.getSource().getData());
			AnyType target = disjointSets.find(edge.getTarget().getData());
			if(source == null) disjointSets.makeSet(source);
			if(target == null) disjointSets.makeSet(target);
			if(disjointSets.isSameSet(source, target)) return true;
			disjointSets.union(source, target);
		}
		return false;
	}
	
	public static <AnyType extends Comparable<AnyType>> int totalConnectedComponents(Graph<AnyType> graph){
		int noOfConnectedComponets = 0;
		GraphTraversal<AnyType> traversal = new DepthFirst<AnyType>(graph);
		HashMap<Integer, Boolean> traversed = new HashMap<Integer, Boolean>();
		
		for(int vertex : graph.vertices()){
			if(traversed.get(vertex) == Boolean.TRUE) continue;
			traversed = traversal.search(vertex);
			noOfConnectedComponets++;
		}
		return noOfConnectedComponets;
	}
	
	public static <AnyType extends Comparable<AnyType>> List<Integer> topologicalSort(Graph<AnyType> graph){
		List<Integer> sortedVertices = new ArrayList<Integer>();
		Graph<AnyType> copy = new AdjList<AnyType>(graph);
		
		Queue<Integer> queue = new Queue<Integer>();
		HashMap<Integer, Vertex<AnyType>> vertices = copy.verticesMap();
		for(Vertex<AnyType> vertex : vertices.values()){
			if(vertex.getInDegree() == 0) 
				queue.push(vertex.getId());
		}
			
		int noOfProcessedVertices = 0;
		
		while(! queue.isEmpty()){
			noOfProcessedVertices++;
			Vertex<AnyType> queueVertex = vertices.get(queue.pop());
			sortedVertices.add(queueVertex.getId());
			for(Vertex<AnyType> neighbor : copy.neighbors(queueVertex)){
				neighbor.setInDegree(neighbor.getInDegree()-1);
				if(neighbor.getInDegree() == 0)
					queue.push(neighbor.getId());
			}
		}
		try{
			if(noOfProcessedVertices < copy.noOfVertices())
				throw new Exception();
		} catch (Exception e){
			e.printStackTrace();
			System.out.println("Not a DAG.");
		}
		return sortedVertices;
	}
	
	@SuppressWarnings("unchecked")
	public static <AnyType extends Comparable<AnyType>> Graph<AnyType> minimumSpammingTree_Kruskal(Graph<AnyType> graph){
		UnionFind<Integer> sets = new UnionFind<Integer>();
		Graph<AnyType> minimumSpammingTree = new AdjList<AnyType>();
		List<GraphEdge<AnyType>> edges = GraphUtils.sort((List<GraphEdge<AnyType>>)graph.edges());
		
		for(int vertex : graph.vertices())
			sets.makeSet(vertex);
		
		for(GraphEdge<AnyType> edge : edges){
			int source = edge.getSource().getId();
			int target = edge.getTarget().getId();
			if(sets.isSameSet(source, target)) continue;
			((AdjList<AnyType>)minimumSpammingTree).addEdge(edge);
			sets.union(source, target);
		}
		
		return minimumSpammingTree;
	}
	
	@SuppressWarnings("unchecked")
	public static Graph<Integer> minimumSpammingTree_Prim(Graph<Integer> graph, Integer source){
		Heap<Vertex<Integer>> heap = new Heap<Vertex<Integer>>(graph.noOfVertices(),Heap.Type.MIN);
		Graph<Integer> minimumSpammingTree = new AdjList<Integer>();
		for(Vertex<Integer> vertex : graph.vertexObjs()){
			vertex.setData(Integer.MAX_VALUE);
			heap.insert(vertex);
		}
		graph.vertex(source).setData(0);
		while(!heap.isEmpty()){
			Vertex<Integer> vertex = (Vertex<Integer>) heap.remove();
			for(Vertex<Integer> neighbor : graph.neighbors(vertex)){
				if(! heap.contains(neighbor)) continue;
				GraphEdge<Integer> edge = (GraphEdge<Integer>)graph.edge(vertex, neighbor);
				if(edge.getCost() < neighbor.getData()){
					((AdjList<Integer>)minimumSpammingTree).addEdge(new GraphEdge<Integer>(vertex,neighbor));
					heap.remove(neighbor);
					neighbor.setData(edge.getCost());
					heap.insert(neighbor);
				}
			}
		}
		return minimumSpammingTree;
	}

	public static <AnyType extends Comparable<AnyType>> List<Integer> path(int start, int end, HashMap<Integer,Integer> pathMap){
		//if(! pathMap.containsKey(start) || ! pathMap.containsKey(end)) return null;
		if(pathMap.get(end) == null) return null;
		return recursePath(start, end, new ArrayList<Integer>(), pathMap);
	}
	
	public static <AnyType extends Comparable<AnyType>>  List<Integer> recursePath(
			int start, 
			int end, 
			List<Integer> path,
			HashMap<Integer,Integer> pathMap){
		if(start == end || end == -1)
			path.add(start);
		else{
			recursePath(start, pathMap.get(end), path, pathMap);
			path.add(end);
		}
		return path;
	}
	
	@SuppressWarnings("unchecked")
	public static Object pathToEdges(Graph<Integer> graph, List<Integer> path){
		if(graph instanceof AdjList){
			List<GraphEdge<Integer>> edges = new ArrayList<GraphEdge<Integer>>();
			for(int i = 0; i < path.size(); i++){
				GraphEdge<Integer> edge = (GraphEdge<Integer>) graph.edge(path.get(i),path.get(i+1));
				edges.add(edge);
			}
			return edges;
		}
		else if(graph instanceof NetworkGraph){
			List<NetworkEdge<Integer>> edges = new ArrayList<NetworkEdge<Integer>>();
			for(int i = 0; i < path.size()-1; i++){
				NetworkEdge<Integer> edge = (NetworkEdge<Integer>) graph.edge(path.get(i),path.get(i+1));
				if(edge == null) edge = new NetworkEdge<Integer>(path.get(i),path.get(i+1));
				edges.add(edge);
			}
			return edges;
		} 
		return null;
	}
}