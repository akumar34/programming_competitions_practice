package graphtraversalcontainers;

import java.util.ArrayList;
import java.util.List;

import graphcontainers.Graph;

public class TopologicalSort<AnyType extends Comparable<AnyType>> extends DepthFirst<AnyType>{
	private List<Integer> sortedVertices = null;
	public TopologicalSort(Graph<AnyType> graph) {
		super(graph);
		sortedVertices = new ArrayList<Integer>();
	}
	
	public List<Integer> sort(){
		search();
		return sortedVertices;
	}
	
	@Override
	protected void processVertex(int vertex){
		sortedVertices.add(0,vertex);
	}
	
	@Override
	protected void processEdge(int source, int target){
		super.processEdge(source, target);
		if(isCycle()){
			try {
				throw new Exception();
			} catch (Exception e) {
				System.out.println("This is not a DAG.");
				e.printStackTrace();
			}
		}
	}
}