package graphtraversalcontainers;

import graphcontainers.Edge;
import graphcontainers.GraphEdge;
import graphcontainers.Graph;
import graphcontainers.AdjMatrix;
import graphcontainers.Vertex;

import java.util.HashMap;
import java.util.List;

import containers.Heap;

public class ShortestPathUtils {
	private static void initialize(List<Vertex<Integer>> vertices, int source, HashMap<Integer,Integer> pathMap){
		for(Vertex<Integer> vertex : vertices){
			if(vertex.getId() == source) vertex.setData(0);
			else vertex.setData(Integer.MAX_VALUE);
			pathMap.put(vertex.getId(), null);
		}
	}
	
	private static void relax(Edge<Integer> edge, HashMap<Integer,Integer> pathMap){
		Vertex<Integer> source = edge.getSource();
		Vertex<Integer> target = edge.getTarget();
		int edgeWeight = ((GraphEdge<Integer>)edge).getCost();
		
		if(source.getData() + edgeWeight < target.getData()){
			target.setData(source.getData() + ((GraphEdge<Integer>)edge).getCost());
			pathMap.put(target.getId(), source.getId());
		}
	}
	
	public static <AnyType extends Comparable<AnyType>> HashMap<Integer,Integer> unweightedShortestPath(Graph<AnyType> graph, int source){
		GraphTraversal<AnyType> traversal = new BreadthFirst<AnyType>(graph);
		traversal.search(source);
		return traversal.pathMap();
	}
	
	@SuppressWarnings("unchecked")
	public static HashMap<Integer, Integer> nonNegWeightedShortestPath(Graph<Integer> graph, int source){
		Heap<Vertex<Integer>> heap = new Heap<Vertex<Integer>>(graph.noOfVertices(),Heap.Type.MIN);
		HashMap<Integer,Integer> pathMap = new HashMap<Integer, Integer>();
		
		for(Vertex<Integer> vertex : graph.vertexObjs()){
			if(vertex.getData() == null) 
				vertex.setData(Integer.MAX_VALUE);
			pathMap.put(vertex.getId(), null);
			heap.insert(vertex);
		}
		graph.vertex(source).setData(0);
		
		while(!heap.isEmpty()){
			Vertex<Integer> vertex = (Vertex<Integer>) heap.remove();
			for(Vertex<Integer> neighbor : graph.neighbors(vertex)){
				if(! heap.contains(neighbor)) continue;
				
				GraphEdge<Integer> edge = (GraphEdge<Integer>)graph.edge(vertex, neighbor);
				
				if(vertex.getData() + edge.getCost() < neighbor.getData()){
					heap.remove(neighbor);
					neighbor.setData(vertex.getData() + edge.getCost());
					pathMap.put(neighbor.getId(), vertex.getId());
					heap.insert(neighbor);
				}
			}
		}
		return pathMap;
	}
	
	@SuppressWarnings("unchecked")
	public static HashMap<Integer, Integer> weightedDirectedAcyclicGraphShortestPath(Graph<Integer> graph, int source){
		List<Integer> vertices = new TopologicalSort<Integer>(graph).sort();
		HashMap<Integer,Integer> pathMap = new HashMap<Integer, Integer>();
		
		initialize(graph.vertexObjs(), source, pathMap);
		
		for(int vertexItem : vertices){
			Vertex<Integer> vertex = graph.vertex(vertexItem);
			for(Vertex<Integer> neighbor : graph.neighbors(vertex)){
				GraphEdge<Integer> edge = (GraphEdge<Integer>)graph.edge(vertex, neighbor);
				relax(edge, pathMap);
			}
		}
		return pathMap;
	}
	
	@SuppressWarnings("unchecked")
	public static HashMap<Integer, Integer> negativeWeightedShortestPath(Graph<Integer> graph, int source){
		HashMap<Integer, Integer> pathMap = new HashMap<Integer, Integer>();
		initialize(graph.vertexObjs(), source, pathMap);
		
		List<GraphEdge<Integer>> edges = (List<GraphEdge<Integer>>)graph.edges();
		for(int i = 1; i <= graph.noOfVertices()-1; i++)
			for(Edge<Integer> edge : edges) relax(edge, pathMap);
		
		for(Edge<Integer> edge : edges)
			if(edge.getSource().getData() + ((GraphEdge<Integer>)edge).getCost() < edge.getTarget().getData())
				try {
					throw new Exception();
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("Contains negative-weight cycle.");
				}
		
		return pathMap;
	}
	
	public static AdjMatrix<Integer> allPairsShortestPathDistance(AdjMatrix<Integer> graph){
		for(int k = 1; k <= graph.noOfVertices(); k++){
			for(int i = 1; i <= graph.noOfVertices(); i++){
				for(int j = 1; j <= graph.noOfVertices(); j++){
					int through_k = graph.cost(i, k) + graph.cost(k, j);
					if(through_k < graph.cost(i,j))
						graph.setEdge(i, j, through_k);
				}
			}
		}
		return graph;
	}
	
	public static HashMap<Integer,Integer> allPairsShortestPath(Graph<Integer> graph){
		HashMap<Integer,Integer> pathMap = null;
		for(int source : graph.vertices())
			pathMap = nonNegWeightedShortestPath(graph, source);
		return pathMap;
	}
}
