package graphtraversalcontainers;

import graphcontainers.Graph;
import java.util.HashMap;

public abstract class GraphTraversal<AnyType extends Comparable<AnyType>> {
	protected Graph<AnyType> graph = null;
	protected HashMap<Integer, Boolean> processed = null;
	protected HashMap<Integer, Boolean> discovered = null;
	protected HashMap<Integer, Integer> pathMap = null;
	
	public GraphTraversal(Graph<AnyType> graph){
		this.graph = graph;
		processed = new HashMap<Integer, Boolean>();
		discovered = new HashMap<Integer, Boolean>();
		pathMap = new HashMap<Integer, Integer>();
		
		for(Integer vertex : this.graph.vertices()){
			processed.put(vertex, false);
			discovered.put(vertex, false);
			pathMap.put(vertex, null);
		}
	}
	
	public HashMap<Integer,Integer> pathMap(){ return pathMap; }
	public abstract HashMap<Integer,Boolean> search(int source);
	protected abstract void processVertex(int vertex);
	protected abstract void processEdge(int source, int target);
}
