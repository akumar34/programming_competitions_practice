package containers;

public class Test {
	public static void main(String [] args){
		Heap<Integer> heap = new Heap<Integer>(10,Heap.Type.MIN);
		heap.insert(45);
		heap.insert(33);
		heap.insert(3);
		heap.insert(17);
		heap.insert(25);
		heap.insert(-34);
		heap.insert(-55);
		heap.insert(4);
		heap.insert(14);
		
		heap.remove();
		System.out.println();
		heap.print();
		
		heap.remove();
		System.out.println();
		heap.print();
	}
	
}
