package containers;

import java.util.Iterator;

public class Stack<AnyType> implements Iterable<AnyType>{
	private DoublyLinkedList<AnyType> list = null;
	
	public Stack(Stack<AnyType> stack){
		list = new DoublyLinkedList<AnyType>();
		Iterator<AnyType> stackItr = stack.iterator();
		while(stackItr.hasNext()) this.push(stackItr.next());
	}
	
	public Stack(){
		list = new DoublyLinkedList<AnyType>();
	}
	
	public boolean contains(AnyType data){
		return list.contains(data);
	}
	
	public void push(AnyType data){
		list.addLast(data);
	}
	
	public AnyType pop(){
		AnyType data = list.getLast();
		list.removeLast();
		return data;
	}
	
	public boolean isEmpty(){
		return list.isEmpty();
	}
	
	public void turtlePop(AnyType data){
		list.remove(data);
		list.addLast(data);
	}
	
	public void remove(AnyType data){
		list.remove(data);
	}
	
	public void print(){
		list.print();
	}

	@SuppressWarnings({ "hiding" })
	private class StackIterator<AnyType> implements Iterator<AnyType>{
		Iterator<AnyType> listIterator = null;
		
		@SuppressWarnings("unchecked")
		public StackIterator(Stack<AnyType> stack) {
			listIterator = (Iterator<AnyType>) list.iterator();
		}

		@Override
		public boolean hasNext() {
			return listIterator.hasNext();
		}

		@Override
		public AnyType next() {
			return listIterator.next();
		}
	}

	@Override
	public Iterator<AnyType> iterator() {
		return new StackIterator<AnyType>(this);
	}
}
