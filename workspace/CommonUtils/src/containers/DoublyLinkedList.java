package containers;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.NoSuchElementException;

public class DoublyLinkedList<AnyType> implements Iterable<AnyType> {
	private LinkedHashMap<AnyType,Node<AnyType>> map = new LinkedHashMap<AnyType,Node<AnyType>>();
	private Node<AnyType> head,tail;
	private int N = 0;
	
	public DoublyLinkedList(){
		head = null;
		tail = head;
	}
	
	public boolean isEmpty(){
		return head == null;
	}
	
	public int size(){
		return N;
	}
	
	public boolean contains(AnyType data){
		for(Node<AnyType> curr = head; curr != tail; curr = curr.next)
			if(curr.data.equals(data)) return true;
		return false;
	}
	
	public AnyType getFirst(){
		return head.data;
	}
	
	public AnyType getLast(){
		return tail.data;
	}

	public void remove(AnyType data){
		for(Node<AnyType> curr = head; curr != null; curr = curr.next){
			if(! curr.data.equals(data)) continue;
			Node<AnyType> prev = curr.prev;
			Node<AnyType> next = curr.next;
			
			if(prev == null && next == null){
				head = head.next;
				tail = tail.prev;
			} else if(prev == null){
				head = head.next;
				next.prev = null;				
			} else if(next == null){
				tail = tail.prev;
				prev.next = null;
			} else{
				prev.next = next;
				next.prev = prev;
			}
			curr = head;
			N--;
			if(curr == null) break;
		}
	}

	public void addFirst(AnyType data){
		if(data == null) throw new NullPointerException();
		Node<AnyType> newNode = new Node<AnyType>();
		newNode.data = data;
		newNode.next = head;
		newNode.prev = null;
		head.prev = newNode;
		head = newNode;
		if(isEmpty()) tail = head;
		N++;
		map.put(data, head);
	}
	public void addLast(AnyType data){
		if(data == null) throw new NullPointerException();
		Node<AnyType> oldTail = tail;
		Node<AnyType> newNode = new Node<AnyType>();
		newNode.data = data;
		tail = newNode;
		tail.prev = oldTail;
		if(isEmpty()) head = tail;
		else oldTail.next = tail;
		N++;
		map.put(data, tail);
	}
	public AnyType removeFirst(){
		if(isEmpty()) throw new NoSuchElementException();
		AnyType data = head.data;
		head = head.next;
		N--;
		map.remove(data);
		return data;
	}
	public AnyType removeLast(){
		if(isEmpty()) throw new NoSuchElementException();
		AnyType data = tail.data;
		tail = tail.prev;
		if(tail == null) head = tail;
		N--;
		map.remove(data);
		return data;
	}
	public void print(){
		for(Node<AnyType> curr = head; curr != null; curr = curr.next) System.out.println(curr.data);
	}
	public Iterator<AnyType> iterator(){
		return new ListIterator();
	}
	private class ListIterator implements Iterator<AnyType>{
		private Node<AnyType> ptr = head;
		int i = 0;
		
		public boolean hasNext(){ 
			return i < N;
		}
		
		public AnyType next(){
			AnyType AnyType = ptr.data;
			if(hasNext()){
				ptr = ptr.next;
				i++;
			}
			else throw new NoSuchElementException();
			return AnyType;
		}
		
		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}
}