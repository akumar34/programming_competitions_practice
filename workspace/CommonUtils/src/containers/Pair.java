package containers;

@SuppressWarnings("rawtypes")
public class Pair<AnyType1,AnyType2> implements Comparable{
	private AnyType1 first;
	private AnyType2 second;
	public Pair(){}
	public Pair(AnyType1 first, AnyType2 second){
		this.first = first;
		this.second = second;
	}
	public void setFirst(AnyType1 first){ this.first = first; }
	public AnyType1 getFirst(){ return first; }
	public void setSecond(AnyType2 second){ this.second = second; }
	public AnyType2 getSecond(){ return second; }
	
	@Override
	public int compareTo(Object o) {
		Pair p = (Pair)o;
		return this.compareTo(p.second);
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((first == null) ? 0 : first.hashCode());
		result = prime * result + ((second == null) ? 0 : second.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pair other = (Pair) obj;
		if (first == null) {
			if (other.first != null)
				return false;
		} else if (!first.equals(other.first))
			return false;
		if (second == null) {
			if (other.second != null)
				return false;
		} else if (!second.equals(other.second))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Pair [first=" + first + ", second=" + second + "]";
	}
}