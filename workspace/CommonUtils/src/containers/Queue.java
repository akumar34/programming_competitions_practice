package containers;

import java.util.Iterator;

public class Queue<AnyType> implements Iterable<AnyType>{
	private DoublyLinkedList<AnyType> list = null;
	
	public Queue(Queue<AnyType> queue){
		list = new DoublyLinkedList<AnyType>();
		Iterator<AnyType> queueItr = queue.iterator();
		while(queueItr.hasNext()) this.push(queueItr.next());
	}
	
	public Queue(){
		list = new DoublyLinkedList<AnyType>();
	}
	
	public boolean contains(AnyType data){
		return list.contains(data);
	}
	
	public void push(AnyType data){
		list.addLast(data);
	}
	
	public AnyType pop(){
		AnyType data = list.getFirst();
		list.removeFirst();
		return data;
	}
	
	public boolean isEmpty(){
		return list.isEmpty();
	}
	
	public void remove(AnyType data){
		list.remove(data);
	}
	
	public void print(){
		list.print();
	}

	@SuppressWarnings({ "hiding" })
	private class QueueIterator<AnyType> implements Iterator<AnyType>{
		Iterator<AnyType> listIterator = null;
		
		@SuppressWarnings("unchecked")
		public QueueIterator(Queue<AnyType> stack) {
			listIterator = (Iterator<AnyType>) list.iterator();
		}

		@Override
		public boolean hasNext() {
			return listIterator.hasNext();
		}

		@Override
		public AnyType next() {
			return listIterator.next();
		}
	}

	@Override
	public Iterator<AnyType> iterator() {
		return new QueueIterator<AnyType>(this);
	}
}