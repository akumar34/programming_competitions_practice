package containers;

import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.common.math.LongMath;

import commonutils.CommonUtils;
import sort.SortUtils;

public class Heap<AnyType extends Comparable<AnyType>> {
	public static enum Type{ MAX,MIN }
	private Type type;
	
	private int curr = 0;
	HashMap<AnyType, Integer> indexMap = new HashMap<AnyType, Integer>();
	
	private List<AnyType> heap = null;
	public Heap(int max, Type type){
		heap = new ArrayList<AnyType>(max);
		heap.add(null);
		this.type = type;
	}
	
	public int leftChild(int index){
		return index*2;
	}
	
	public int rightChild(int index){
		return index*2 + 1;
	}
	
	public int parent(int index){
		if(index == 1) return 1;
		return index/2;
	}
	
	public boolean isLeaf(int index){
		return index > LongMath.log2(size(), RoundingMode.CEILING);
	}
	
	public boolean hasLeftChild(int index){
		return heap.get(leftChild(index)) != null;
	}
	
	public boolean hasRightChild(int index){
		return heap.get(rightChild(index)) != null;
	}
	
	public int size(){
		return curr;
	}
	
	public boolean isEmpty(){
		return size() == 0;
	}
	
	public boolean contains(AnyType data){
		return indexMap.containsKey(data);
	}
	
	public void heapUp(int index){
		switch(type){
			case MAX:
				while( SortUtils.less(heap.get(parent(index)), heap.get(index)) ){
					SortUtils.exch(heap, index, parent(index));
					indexMap.put(heap.get(parent(index)), parent(index));
					indexMap.put(heap.get(index),index);
					index = parent(index);
				}
				break;
			case MIN:
				while( SortUtils.less(heap.get(index), heap.get(parent(index))) ) {
					SortUtils.exch(heap, index, parent(index));
					indexMap.put(heap.get(parent(index)), parent(index));
					indexMap.put(heap.get(index),index);
					index = parent(index);
				}
				break;
		}
	}
	
	public void heapDown(int index){
		switch(type){
			case MAX:
				while(isSwapNeeded(index)){
					if(SortUtils.less(heap.get(leftChild(index)), heap.get(rightChild(index)))){
						SortUtils.exch(heap, index, rightChild(index));
						indexMap.put(heap.get(index),index);
						indexMap.put(heap.get(rightChild(index)), rightChild(index));
						index = rightChild(index);
					} else {
						SortUtils.exch(heap, index, leftChild(index));
						indexMap.put(heap.get(index),index);
						indexMap.put(heap.get(leftChild(index)), leftChild(index));
						index = leftChild(index);
					}
				}
				break;
			case MIN:
				while(isSwapNeeded(index)){
					if(SortUtils.less(heap.get(rightChild(index)), heap.get(leftChild(index)))){
						SortUtils.exch(heap, index, rightChild(index));
						indexMap.put(heap.get(index),index);
						indexMap.put(heap.get(rightChild(index)), rightChild(index));
						index = rightChild(index);
					} else {
						SortUtils.exch(heap, index, leftChild(index));
						indexMap.put(heap.get(index),index);
						indexMap.put(heap.get(leftChild(index)), leftChild(index));
						index = leftChild(index);
					}
				}
				break;
		}
	}
	
	private boolean isSwapNeeded(int index){
		if( isLeaf(index)) return false;
		if(! hasLeftChild(index)) return false;
		if(! hasRightChild(index)) return false;
		switch(type){
			case MAX:
					return SortUtils.less(heap.get(index), CommonUtils.max(heap.get(leftChild(index)), heap.get(rightChild(index))));
			case MIN:
					return SortUtils.less(CommonUtils.min(heap.get(leftChild(index)), heap.get(rightChild(index))), heap.get(index));
		}
		return false;
	}
	
	public void insert(AnyType data){
		curr++;
		if(heap.size() > curr) 
			heap.set(curr,data);
		else
			heap.add(data);
		int index = curr;
		indexMap.put(heap.get(curr), curr);
		heapUp(index);
	}

	public void remove(AnyType data){
		Integer foundIndex = indexMap.get(data);
		if(foundIndex == null) return;
		SortUtils.exch(heap, foundIndex, curr);
		indexMap.put(heap.get(foundIndex), foundIndex);
		indexMap.put(heap.get(curr),curr);
		indexMap.remove(heap.get(curr));
		heap.set(curr,null);
		curr--;
		if(isEmpty()) return;
		int index = foundIndex;
		heapDown(index);
	}

	public AnyType remove(){
		AnyType data = heap.get(1);
		remove(heap.get(1));
		return data;
	}

	public void print(){
		for(AnyType data : indexMap.keySet())
			System.out.println("[" + data + ")[" + indexMap.get(data) + ")");
	}

}
