package unionfindcontainers;

public class Test {
	public static void main(String [] args){
		UnionFind<Integer> disjointSets = new UnionFind<Integer>();
		disjointSets.makeSet(1);
		disjointSets.makeSet(2);
		disjointSets.makeSet(3);
		disjointSets.makeSet(4);
		disjointSets.makeSet(5);
		disjointSets.makeSet(6);
		
		disjointSets.union(1, 2);
		disjointSets.union(2, 3);
		
		disjointSets.union(4, 5);
		disjointSets.union(5, 6);
		
		disjointSets.union(3, 4);
		
		disjointSets.print();
	}

}
