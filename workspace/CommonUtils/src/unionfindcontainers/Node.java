package unionfindcontainers;

public class Node<AnyType>
{
	public AnyType data;
	public int rank;
	public Node<AnyType> parent;
   
   public Node() {
	   this.data = null;
	   this.rank = 0;
	   this.parent = null;
   }
   
   public Node(AnyType data){
	   this.data = data;
	   this.rank = 0;
   }
   
   public Node(AnyType data, Node<AnyType> parent)
   {
      this.data = data;
      this.parent = parent;
      this.rank = 0;
   }
}