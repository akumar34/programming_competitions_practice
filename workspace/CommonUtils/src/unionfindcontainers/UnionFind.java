package unionfindcontainers;

import java.util.HashMap;

public class UnionFind<AnyType>{
	private HashMap<AnyType, Node<AnyType>> map = null;
	
	public UnionFind(){
		map = new HashMap<AnyType, Node<AnyType>>();
	}
	
	public void makeSet(AnyType data){
		Node<AnyType> node = new Node<AnyType>(data);
		node.rank = 1;
		node.parent = null;
		map.put(data, node);	
	}
	
	public void union(AnyType data1, AnyType data2){
		link(find(data1), find(data2));
	}
	
	public AnyType find(AnyType data){
		Node<AnyType> root = map.get(data);
		if(root == null) return null;
		
		while(root.parent != null) 
			root = root.parent;
		
		Node<AnyType> curr = map.get(data);
		if(curr == root) return root.data;
		
		while(curr.parent != root){
			Node<AnyType> temp = new Node<AnyType>(curr.data, curr.parent);
			curr.parent = root;
			curr = temp.parent;
		}

		return root.data;
	}
	
	private void link(AnyType data1, AnyType data2){
		Node<AnyType> node1 = map.get(data1);
		Node<AnyType> node2 = map.get(data2);
		
		if(node1.rank >= node2.rank){
			node2.parent = node1;
			node1.rank += node2.rank;
			node2.rank = 1;
			return;
		}
		node1.parent = node2;
		node2.rank += node1.rank;
		node1.rank = 1;
	}
	
	public boolean isSameSet(AnyType data1, AnyType data2){
		return find(data1) == find(data2);
	}
	
	public void print(){
		for(AnyType data : map.keySet()){
			System.out.println("[" + data + "][" + map.get(data).rank + "][" + find(data) + "]");
		}
	}
}