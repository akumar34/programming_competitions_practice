package graphcontainers;

import java.util.Comparator;

public class EdgeComparator<AnyType extends Comparable<AnyType>> implements Comparator<Edge<AnyType>>{
	@Override
	public int compare(Edge<AnyType> o1, Edge<AnyType> o2) {
		GraphEdge<AnyType> edge1 = (GraphEdge<AnyType>)o1;
		GraphEdge<AnyType> edge2 = (GraphEdge<AnyType>)o2;
		Integer cost1 = Integer.valueOf(edge1.getCost());
		Integer cost2 = Integer.valueOf(edge2.getCost());
		return cost1.compareTo(cost2);	
	}
}
