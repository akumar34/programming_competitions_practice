package graphcontainers;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class AdjList<AnyType extends Comparable<AnyType>> extends Graph<AnyType> {
	protected LinkedHashMap<Vertex<AnyType>, List<GraphEdge<AnyType>>> neighbors = null;
	protected List<GraphEdge<AnyType>> edges = null;
	
	public AdjList(){
		super();
		neighbors = new LinkedHashMap<Vertex<AnyType>, List<GraphEdge<AnyType>>>();
		edges = new ArrayList<GraphEdge<AnyType>>();
	}
	
	@SuppressWarnings("unchecked")
	public AdjList(AdjList<AnyType> graph){
		super(graph);
		neighbors = new LinkedHashMap<Vertex<AnyType>, List<GraphEdge<AnyType>>>();
		edges = new ArrayList<GraphEdge<AnyType>>();
		
		List<GraphEdge<AnyType>> edges = (List<GraphEdge<AnyType>>)graph.edges();
		for(GraphEdge<AnyType> edge : edges) 
			this.addEdge(new GraphEdge<AnyType>(edge));
	}
	
	@SuppressWarnings("unchecked")
	public AdjList(Graph<AnyType> graph){
		super(graph);
		neighbors = new LinkedHashMap<Vertex<AnyType>, List<GraphEdge<AnyType>>>();
		edges = new ArrayList<GraphEdge<AnyType>>();
		
		List<GraphEdge<AnyType>> edges = (List<GraphEdge<AnyType>>)graph.edges();
		for(GraphEdge<AnyType> edge : edges) 
			this.addEdge(new GraphEdge<AnyType>(edge));
	}
	
	public void addEdge(GraphEdge<AnyType> edge) {
		int source = edge.getSource().getId();
		int target = edge.getTarget().getId();
		int cost = edge.getCost();
		addEdge(source, target, cost);
	}
	
	@SuppressWarnings("unchecked")
	public void addEdge(int source, int target, int cost){
		GraphEdge<AnyType> edge = (GraphEdge<AnyType>)createNewEdge(source, target);
		edge.setCost(cost);
		edges.add(edge);
		noOfEdges++;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void addEdge(int source, int target) {
		edges.add((GraphEdge<AnyType>)createNewEdge(source, target));
		noOfEdges++;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object createNewEdge(int source, int target) {
		Vertex<AnyType> sourceVertex = vertex(source);
		Vertex<AnyType> targetVertex = vertex(target);
		
		sourceVertex.setOutDegree(sourceVertex.getOutDegree() + 1);
		targetVertex.setInDegree(targetVertex.getInDegree() + 1);
		
		vertices.put(source, sourceVertex);
		vertices.put(target, targetVertex);
		
		GraphEdge<AnyType> edge = (GraphEdge<AnyType>)edge(source, target);
		
		List<GraphEdge<AnyType>> sourceEdges = neighbors.get(sourceVertex);
		if(sourceEdges == null) sourceEdges = new ArrayList<GraphEdge<AnyType>>();
		sourceEdges.add(edge);
		neighbors.put(sourceVertex, sourceEdges);
		
		return edge;
	}

	@Override
	public Object edge(int source, int target) {
		GraphEdge<AnyType> edge = null;
		edge = new GraphEdge<AnyType>(vertex(source), vertex(target));
		
		Vertex<AnyType> sourceVertex = edge.getSource();
		List<GraphEdge<AnyType>> sourceEdges = neighbors.get(sourceVertex);
		if(sourceEdges == null) sourceEdges = new ArrayList<GraphEdge<AnyType>>();
		
		for(GraphEdge<AnyType> sourceEdge : sourceEdges)
			if(sourceEdge.equals(edge)) return sourceEdge;
		return edge;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void removeEdge(int source, int target) {
		GraphEdge<AnyType> edge = (GraphEdge<AnyType>)edge(source, target);
		neighbors.get(edge.getSource()).remove(edge);
		edges.remove(edge);
		noOfEdges--;	
	}

	@Override
	public List<Integer> neighbors(int source) {
		List<GraphEdge<AnyType>> edges = neighbors.get(vertex(source));
		if(edges == null) return new ArrayList<Integer>();
		List<Integer> targets = new ArrayList<Integer>();
		for(GraphEdge<AnyType> edge : edges) targets.add(edge.getTarget().getId());
		return targets;
	}

	@Override
	public Object edges() {
		return edges;
	}

	@Override
	public void print() {
		for(Vertex<AnyType> vertex : neighbors.keySet()){
			System.out.print("[" + vertex.getData() + "]");
			for(Edge<AnyType> edge : neighbors.get(vertex)){
				System.out.print("[" + edge.getSource().getId() + "," + edge.getTarget().getId() + "]");
			}
			System.out.println();
		}
	}
}