package graphcontainers;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public abstract class Graph<AnyType extends Comparable<AnyType>> {
	protected LinkedHashMap<Integer,Vertex<AnyType>> vertices = null;
	protected int noOfEdges;
	
	public Graph(Graph<AnyType> graph){
		vertices = new LinkedHashMap<Integer, Vertex<AnyType>>();
		noOfEdges = 0;
	}
	
	public Graph(){
		 vertices = new LinkedHashMap<Integer, Vertex<AnyType>>();
		 noOfEdges = 0;
	}
	
	public abstract void addEdge(int source, int target);
	
	public abstract Object createNewEdge(int source, int target);

	public Vertex<AnyType> vertex(int vertex){
		Vertex<AnyType> vertexObj = vertices.get(vertex);
		if(vertexObj == null) vertexObj = new Vertex<AnyType>(vertex,null);
		return vertexObj;
	}
	
	public abstract Object edge(int source, int target);

	public Object edge(Vertex<AnyType> source, Vertex<AnyType> target){
		return edge(source.getId(), target.getId());
	}
	
	public void removeEdge(Edge<AnyType> edge){
		removeEdge(edge.getSource().getId(), edge.getTarget().getId());
	}
	
	public abstract void removeEdge(int source, int target);
	
	public abstract List<Integer> neighbors(int source);

	public List<Vertex<AnyType>> neighbors(Vertex<AnyType> source){
		List<Vertex<AnyType>> targets = new ArrayList<Vertex<AnyType>>();
		List<Integer> neighbors = neighbors(source.getId());
		for(int neighbor : neighbors){
			Vertex<AnyType> neighborObj = vertex(neighbor);
			targets.add(neighborObj);
		}
		return targets;
	}
	
	public List<Integer> vertices(){
		List<Integer> verticesList = new ArrayList<Integer>();
		verticesList.addAll(vertices.keySet());
		return verticesList;
	}
	
	public List<Vertex<AnyType>> vertexObjs(){
		return new ArrayList<Vertex<AnyType>>(verticesMap().values());
	}
	
	public int noOfEdges(){
		return noOfEdges;
	}
	
	public int noOfVertices(){
		return vertices.keySet().size();
	}
	
	public LinkedHashMap<Integer, Vertex<AnyType>> verticesMap(){
		return vertices;
	}
	
	public abstract Object edges();
	
	public abstract void print();
}
