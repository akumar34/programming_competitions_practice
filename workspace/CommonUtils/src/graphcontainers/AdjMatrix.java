package graphcontainers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

public class AdjMatrix<AnyType extends Comparable<AnyType>> extends Graph<AnyType>{
	protected LinkedHashMap<Vertex<AnyType>, List<GraphEdge<AnyType>>> neighbors = null;
	protected List<GraphEdge<AnyType>> edges = null;
	private int adjMatrix[][] = null;
	
	public AdjMatrix(int noOfVertices){
		super();
		neighbors = new LinkedHashMap<Vertex<AnyType>, List<GraphEdge<AnyType>>>();
		edges = new ArrayList<GraphEdge<AnyType>>();
		
		this.adjMatrix = new int[noOfVertices+1][noOfVertices+1];
		
		for(int i = 1; i <= noOfVertices(); i++)
			for(int j = 1; j <= noOfVertices(); j++)
				this.adjMatrix[i][j] = Integer.MAX_VALUE;
	}
	
	public int cost(int source, int target){
		return adjMatrix[source][target];
	}
	
	public void setEdge(int source, int target, int cost){
		adjMatrix[source][target] = cost;
	}
	
	public void addEdge(GraphEdge<AnyType> edge) {
		int source = edge.getSource().getId();
		int target = edge.getTarget().getId();
		int cost = edge.getCost();
		addEdge(source, target, cost);
	}
	
	@SuppressWarnings("unchecked")
	public void addEdge(int source, int target, int cost){
		GraphEdge<AnyType> edge = (GraphEdge<AnyType>)createNewEdge(source, target);
		edge.setCost(cost);
		edges.add(edge);
		noOfEdges++;
		setEdge(source, target, cost);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void addEdge(int source, int target) {
		GraphEdge<AnyType> edge = (GraphEdge<AnyType>)createNewEdge(source, target);
		edges.add(edge);
		noOfEdges++;
		setEdge(source, target, edge.getCost());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Object createNewEdge(int source, int target) {
		Vertex<AnyType> sourceVertex = vertex(source);
		Vertex<AnyType> targetVertex = vertex(target);
		
		sourceVertex.setOutDegree(sourceVertex.getOutDegree() + 1);
		targetVertex.setInDegree(targetVertex.getInDegree() + 1);
		
		vertices.put(source, sourceVertex);
		vertices.put(target, targetVertex);
		
		GraphEdge<AnyType> edge = (GraphEdge<AnyType>)edge(source, target);
		
		List<GraphEdge<AnyType>> sourceEdges = neighbors.get(sourceVertex);
		if(sourceEdges == null) sourceEdges = new ArrayList<GraphEdge<AnyType>>();
		sourceEdges.add(edge);
		neighbors.put(sourceVertex, sourceEdges);
		
		return edge;
	}

	@Override
	public Object edge(int source, int target) {
		GraphEdge<AnyType> edge = null;
		edge = new GraphEdge<AnyType>(vertex(source), vertex(target));
		
		Vertex<AnyType> sourceVertex = edge.getSource();
		List<GraphEdge<AnyType>> sourceEdges = neighbors.get(sourceVertex);
		if(sourceEdges == null) sourceEdges = new ArrayList<GraphEdge<AnyType>>();
		
		for(GraphEdge<AnyType> sourceEdge : sourceEdges)
			if(sourceEdge.equals(edge)) return sourceEdge;
		return edge;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void removeEdge(int source, int target) {
		GraphEdge<AnyType> edge = (GraphEdge<AnyType>)edge(source, target);
		neighbors.get(edge.getSource()).remove(edge);
		edges.remove(edge);
		noOfEdges--;	
	}

	@Override
	public List<Integer> neighbors(int source) {
		List<GraphEdge<AnyType>> edges = neighbors.get(vertex(source));
		if(edges == null) return new ArrayList<Integer>();
		List<Integer> targets = new ArrayList<Integer>();
		for(GraphEdge<AnyType> edge : edges) targets.add(edge.getTarget().getId());
		return targets;
	}

	@Override
	public Object edges() {
		return edges;
	}

	public boolean contains(int source, int target){
		return adjMatrix[source][target] != Integer.MAX_VALUE;
	}
	
	@Override
	public String toString() {
		return "GraphAdjMatrix [noOfEdges=" + noOfEdges + ", noOfVertices="
				+ noOfVertices() + ", adjMatrix=" + Arrays.toString(adjMatrix)
				+ "]";
	}
	
	public void print(){
		for(int i = 1; i <= noOfVertices(); i++)
			for(int j = 1; j <= noOfVertices(); j++)
				System.out.print("[" + i + "][" + j + "][" + adjMatrix[i][j] + "]");
	}
}
