package graphcontainers;

public class GraphEdge<AnyType extends Comparable<AnyType>> extends Edge<AnyType> {
	private Integer cost;
	
	public GraphEdge(){
		super();
		cost = 0;
	}
	
	public GraphEdge(GraphEdge<AnyType> edge){
		super(edge);
		this.cost = new Integer(edge.getCost());
	}
	
	public GraphEdge(Integer source, Integer target){
		super(source, target);
	}
	
	public GraphEdge(Vertex<AnyType> source, Vertex<AnyType> target){
		super(source, target);
		this.cost = 0;
	}
	
	public GraphEdge(Vertex<AnyType> source, Vertex<AnyType> target, Integer cost){
		super(source, target);
		this.cost = cost;
	}
	
	public Integer getCost() {
		return cost;
	}
	
	public void setCost(Integer cost) {
		this.cost = cost;
	}
	
	@Override
	public String toString() {
		return "Edge [source=" + source + ", target=" + target + ", cost="
				+ cost + "]";
	}
}
