package graphcontainers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GraphUtils{
	public static <AnyType extends Comparable<AnyType>> List<GraphEdge<AnyType>> sort(List<GraphEdge<AnyType>> edges){
		List<GraphEdge<AnyType>> copy = new ArrayList<GraphEdge<AnyType>>(edges);
		Collections.sort(copy, new EdgeComparator<AnyType>());
		
		return copy;
	}
}
