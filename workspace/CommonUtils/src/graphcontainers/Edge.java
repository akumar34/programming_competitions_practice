package graphcontainers;

public class Edge<AnyType extends Comparable<AnyType>> /*implements Comparable<Edge<AnyType>>*/{
	public Vertex<AnyType> source;
	public Vertex<AnyType> target;
	
	public Edge(){
		source = new Vertex<AnyType>();
		target = new Vertex<AnyType>();
	}
	
	public Edge(Edge<AnyType> edge){
		this.source = new Vertex<AnyType>(edge.getSource());
		this.target = new Vertex<AnyType>(edge.getTarget());
	}
	
	public Edge(Integer source, Integer target){
		this.source = new Vertex<AnyType>(source,null);
		this.target = new Vertex<AnyType>(target,null);
	}
	
	public Edge(Vertex<AnyType> source, Vertex<AnyType> target){
		this.source = source;
		this.target = target;
	}
	
	public Vertex<AnyType> getSource() {
		return source;
	}
	
	public void setSource(Vertex<AnyType> source) {
		this.source = source;
	}
	
	public Vertex<AnyType> getTarget() {
		return target;
	}
	
	public void setTarget(Vertex<AnyType> target) {
		this.target = target;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		Edge<AnyType> other = (Edge<AnyType>) obj;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		if (target == null) {
			if (other.target != null)
				return false;
		} else if (!target.equals(other.target))
			return false;
		return true;
	}
	/*@Override
	public int compareTo(Edge<AnyType> o) {
		Edge<AnyType> edge = (Edge<AnyType>)o;
		Integer cost1= this.getCost();
		Integer cost2 = edge.getCost();
		return cost1.compareTo(cost2);
	}*/
}
