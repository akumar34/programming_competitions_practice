package graphcontainers;

public class Vertex<AnyType extends Comparable<AnyType>> implements Comparable<Vertex<AnyType>> {
	private int id;
	private int inDegree;
	private int outDegree;
	private AnyType data;
	private boolean isDiscovered;
	
	public Vertex(){
		id = -1;
		inDegree = 0;
		outDegree = 0;
		isDiscovered = false;
		data = null;
	}
	
	public Vertex(Vertex<AnyType> vertex){
		this.id = vertex.getId();
		this.inDegree = vertex.getInDegree();
		this.outDegree = vertex.getOutDegree();
		this.data = vertex.getData();
	}
	
	public Vertex(int id, AnyType data){
		this.id = id;
		this.inDegree = 0;
		this.outDegree = 0;
		this.data = data;
	}
	
	public Vertex(int inDegree, int outDegree){
		this.id = -1;
		this.inDegree = inDegree;
		this.outDegree = outDegree;
		this.data = null;
	}
	
	public Vertex(int inDegree, int outDegree, int id, AnyType data){
		this.id = id;
		this.inDegree = inDegree;
		this.outDegree = outDegree;
		this.data = data;
	}
	
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public int getInDegree() {
		return inDegree;
	}
	
	public void setInDegree(int inDegree) {
		this.inDegree = inDegree;
	}
	
	public int getOutDegree() {
		return outDegree;
	}
	
	public void setOutDegree(int outDegree) {
		this.outDegree = outDegree;
	}
	
	public AnyType getData() {
		return data;
	}
	
	public void setData(AnyType data) {
		this.data = data;
	}
	
	public boolean getIsDiscovered(){
		return isDiscovered;
	}
	
	public void setIsDiscovered(boolean isDiscovered){
		this.isDiscovered = isDiscovered;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vertex<AnyType> other = (Vertex<AnyType>) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Vertex [id=" + id + ", inDegree=" + inDegree + ", outDegree=" + outDegree
				+ ", data=" + data + "]";
	}
	
	@Override
	public int compareTo(Vertex<AnyType> o) {
		Vertex<AnyType> vertex = (Vertex<AnyType>)o;
		AnyType data1 = this.getData();
		AnyType data2 = vertex.getData();
		return data1.compareTo(data2);
	} 
}
