package networkflowcontainers;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import graphcontainers.Graph;
import graphcontainers.Vertex;

public class NetworkGraph<AnyType extends Comparable<AnyType>>  extends Graph<AnyType> {
	protected LinkedHashMap<Vertex<AnyType>, List<NetworkEdge<AnyType>>> neighbors = null;
	protected List<NetworkEdge<AnyType>> edges = null;
	
	@SuppressWarnings("unchecked")
	public NetworkGraph(NetworkGraph<AnyType> graph){
		super(graph);
		neighbors = new LinkedHashMap<Vertex<AnyType>, List<NetworkEdge<AnyType>>>();
		edges = new ArrayList<NetworkEdge<AnyType>>();
		
		List<NetworkEdge<AnyType>> edges = (List<NetworkEdge<AnyType>>)graph.edges();
		for(NetworkEdge<AnyType> edge : edges) 
			this.addEdge(new NetworkEdge<AnyType>(edge));
	}
	
	@SuppressWarnings("unchecked")
	public NetworkGraph(Graph<AnyType> graph){
		super(graph);
		neighbors = new LinkedHashMap<Vertex<AnyType>, List<NetworkEdge<AnyType>>>();
		edges = new ArrayList<NetworkEdge<AnyType>>();
		
		List<NetworkEdge<AnyType>> edges = (List<NetworkEdge<AnyType>>)graph.edges();
		for(NetworkEdge<AnyType> edge : edges) 
			this.addEdge(new NetworkEdge<AnyType>(edge));
	}
	
	public NetworkGraph(){
		super();
		neighbors = new LinkedHashMap<Vertex<AnyType>, List<NetworkEdge<AnyType>>>();
		edges = new ArrayList<NetworkEdge<AnyType>>();
	}
	
	public void addEdge(NetworkEdge<AnyType> edge){
		int source = edge.getSource().getId();
		int target = edge.getTarget().getId();
		int capacity = edge.getCapacity();
		int flow = edge.getFlow();
		addEdge(source, target, capacity, flow);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void addEdge(int source, int target) {
		NetworkEdge<AnyType> edge = (NetworkEdge<AnyType>)createNewEdge(source, target);
		if(edge == null) edge = new NetworkEdge<AnyType>(source,target);
		edges.add(edge);
		noOfEdges++;
	}
	
	@SuppressWarnings("unchecked")
	public void addEdge(int source, int target, int capacity, int flow){
		NetworkEdge<AnyType> edge = (NetworkEdge<AnyType>)createNewEdge(source, target);
		if(edge == null) edge = new NetworkEdge<AnyType>(source,target);
		edge.setCapacity(capacity);
		edge.setFlow(flow);
		edges.add(edge);
		noOfEdges++;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object createNewEdge(int source, int target) {
		Vertex<AnyType> sourceVertex = vertex(source);
		Vertex<AnyType> targetVertex = vertex(target);
		
		sourceVertex.setOutDegree(sourceVertex.getOutDegree() + 1);
		targetVertex.setInDegree(targetVertex.getInDegree() + 1);
		
		vertices.put(source, sourceVertex);
		vertices.put(target, targetVertex);
		
		NetworkEdge<AnyType> edge = (NetworkEdge<AnyType>)edge(source, target);
		if(edge == null) edge = new NetworkEdge<AnyType>(source,target);
		
		List<NetworkEdge<AnyType>> sourceEdges = neighbors.get(sourceVertex);
		if(sourceEdges == null) sourceEdges = new ArrayList<NetworkEdge<AnyType>>();
		sourceEdges.add(edge);
		neighbors.put(sourceVertex, sourceEdges);
		
		return edge;
	}
	
	@Override
	public Object edge(int source, int target) {
		Vertex<AnyType> sourceVertex = vertex(source);
		List<NetworkEdge<AnyType>> sourceEdges = neighbors.get(sourceVertex);
		if(sourceEdges == null) sourceEdges = new ArrayList<NetworkEdge<AnyType>>();
		
		for(NetworkEdge<AnyType> sourceEdge : sourceEdges){
			if(sourceEdge.equals(new NetworkEdge<AnyType>(vertex(source), vertex(target)))) return sourceEdge;
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void removeEdge(int source, int target) {
		NetworkEdge<AnyType> edge = (NetworkEdge<AnyType>)edge(source, target);
		neighbors.get(edge.getSource()).remove(edge);
		edges.remove(edge);
		noOfEdges--;	
	}

	@Override
	public List<Integer> neighbors(int source) {
		List<NetworkEdge<AnyType>> edges = neighbors.get(vertex(source));
		if(edges == null) return new ArrayList<Integer>();
		List<Integer> targets = new ArrayList<Integer>();
		for(NetworkEdge<AnyType> edge : edges) targets.add(edge.getTarget().getId());
		return targets;
	}

	@Override
	public Object edges() {
		return edges;
	}

	@Override
	public void print() {
		for(Vertex<AnyType> vertex : neighbors.keySet()){
			System.out.print("[" + vertex.getData() + "]");
			for(NetworkEdge<AnyType> edge : neighbors.get(vertex)){
				System.out.print("[" + edge.getSource().getId() + "," + edge.getTarget().getId() + "][" + edge.getCapacity() + "][" + edge.getFlow() + "]");
			}
			System.out.println();
		}
	}
}
