package networkflowcontainers;

import java.util.ArrayList;
import java.util.List;
import graphtraversalcontainers.GraphTraversalUtils;
import graphtraversalcontainers.ShortestPathUtils;

public class NetworkFlowUtils {
	@SuppressWarnings("unchecked")
	public static NetworkGraph<Integer> maximumFlow(NetworkGraph<Integer> graph, int source, int target){
		List<Integer> path = new ArrayList<Integer>();
		
		for(NetworkEdge<Integer> edge : (List<NetworkEdge<Integer>>)graph.edges())
			edge.setFlow(0);

		while(path != null){
			NetworkGraph<Integer> residualGraph = new NetworkGraph<Integer>();
			residualGraph = buildResidualGraph(residualGraph,graph);
			path = GraphTraversalUtils.path(source, target, ShortestPathUtils.unweightedShortestPath(residualGraph, source));
			
			if(path == null) continue;
			
			List<NetworkEdge<Integer>> pathEdges = (List<NetworkEdge<Integer>>)GraphTraversalUtils.pathToEdges(residualGraph, path);
			int minCapacity = pathEdges.get(0).getCapacity();
			for(NetworkEdge<Integer> residualEdge : pathEdges)
				if(residualEdge.getCapacity() < minCapacity) 
					minCapacity = residualEdge.getCapacity();
			
			for(NetworkEdge<Integer> residualEdge : pathEdges){
				NetworkEdge<Integer> forwardEdge = (NetworkEdge<Integer>)graph.edge(residualEdge.getSource().getId(), residualEdge.getTarget().getId());
				NetworkEdge<Integer> backwardEdge = (NetworkEdge<Integer>)graph.edge(residualEdge.getTarget().getId(), residualEdge.getSource().getId());
				if(forwardEdge != null)
					forwardEdge.setFlow(forwardEdge.getFlow() + minCapacity);
				else
					backwardEdge.setFlow(backwardEdge.getFlow() + minCapacity);
			}
		}
		return graph;
	}
	
	@SuppressWarnings("unchecked")
	public static NetworkGraph<Integer> buildResidualGraph(NetworkGraph<Integer> residualGraph, NetworkGraph<Integer> graph){
		List<NetworkEdge<Integer>> edges = (List<NetworkEdge<Integer>>)graph.edges();
		for(NetworkEdge<Integer> edge : edges){
			if(edge.getFlow() > 0)
				residualGraph.addEdge(
					edge.getTarget().getId(), edge.getSource().getId(),edge.getFlow(), 0);
			if(edge.getFlow() < edge.getCapacity())
				residualGraph.addEdge(
						edge.getSource().getId(), edge.getTarget().getId(),edge.getCapacity() - edge.getFlow(), 0);				
			
		}
		return residualGraph;
	}
}
