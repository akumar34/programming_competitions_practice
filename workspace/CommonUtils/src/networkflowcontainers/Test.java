package networkflowcontainers;

public class Test {
	public static void main(String [] args){
		NetworkGraph<Integer> graph = new NetworkGraph<Integer>();
		
		graph.addEdge(1,2,16,0);
		graph.addEdge(1,3,13,0);
		graph.addEdge(2,3,10,0);
		graph.addEdge(3,2,4,0);
		graph.addEdge(2,4,12,0);
		graph.addEdge(4,3,9,0);
		graph.addEdge(5,4,7,0);
		graph.addEdge(5,6,4,0);
		graph.addEdge(4,6,20,0);
		graph.addEdge(3,5,14,0);
		
		graph = NetworkFlowUtils.maximumFlow(graph, 1, 6);
		
		graph.print();
	}
}
