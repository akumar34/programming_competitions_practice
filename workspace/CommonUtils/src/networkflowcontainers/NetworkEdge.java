package networkflowcontainers;

import graphcontainers.Edge;
import graphcontainers.Vertex;

public class NetworkEdge<AnyType extends Comparable<AnyType>> extends Edge<AnyType> {
	private int capacity;
	private int flow;
	
	public NetworkEdge(){
		super();
		this.capacity = 0;
		this.flow = 0;
	}
	
	public NetworkEdge(NetworkEdge<AnyType> edge){
		super(edge);
		this.capacity = edge.getCapacity();
		this.flow = edge.getFlow();
	}
	
	public NetworkEdge(Vertex<AnyType> source, Vertex<AnyType> target){
		super(source, target);
		this.capacity = 0;
		this.flow = 0;
	}
	
	public NetworkEdge(Vertex<AnyType> source, Vertex<AnyType> target, int capacity, int flow){
		super(source, target);
		this.capacity = capacity;
		this.flow = flow;
	}
	
	public NetworkEdge(Integer source, Integer target){
		super(source, target);
	}
	
	public int getCapacity() {
		return capacity;
	}
	
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	
	public int getFlow() {
		return flow;
	}
	
	public void setFlow(int flow) {
		this.flow = flow;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		Edge<AnyType> other = (Edge<AnyType>) obj;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		if (target == null) {
			if (other.target != null)
				return false;
		} else if (!target.equals(other.target))
			return false;
		return true;
	}
}