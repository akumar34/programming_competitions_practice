package comp_prog;

import commonutils.TextReader;
import containers.Pair;

public class problem100 {
	public static void main(String [] args){
		TextReader.initialize("/media/beanz/Data/programming_competitions_practice/prog_comp_samples/100.inp");
		
		String line = null;
		while( (line = TextReader.readNextLine()) != null){
			String[] tokens = line.split(" ");
			int i = Integer.valueOf(tokens[0]);
			int j = Integer.valueOf(tokens[1]);
			
			Pair<Integer,Integer> range = range(i, j);
			int max = 0;
			for(int index = range.getFirst(); index <= range.getSecond(); index++){
				int cnt = compute(index);
				if(cnt > max) max = cnt;
			}
			System.out.println(i + " " + j + " " + max);
		}
	}
	
	public static int compute(long n){
		int cnt = 1;
		while(n != 1){
			if(n%2 == 1){
				n = 3*n + 1;
				n = n >> 1;
				cnt += 2;
			} else {
				n = n >> 1;
				cnt++;
			}
		}
		return cnt;
	}
	
	public static Pair<Integer,Integer> range(int i, int j){
		if(i < j) return new Pair<Integer,Integer>(i,j);
		return new Pair<Integer,Integer>(j,i);
	}
}