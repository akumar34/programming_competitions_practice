package prog_comp_challenges;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import commonutils.CommonUtils;
import commonutils.TextReader;

public class problem168 {
	public static void main(String [] args){
		TextReader.initialize("/media/beanz/Data/programming_competitions_practice/samples/110108.inp");
		int totalCases = Integer.valueOf(TextReader.readNextLine());
		TextReader.readNextLine();
		for(int currCase = 0; currCase < totalCases; currCase++){
			HashMap<Integer, BigDecimal> percentVotes = new HashMap<Integer, BigDecimal>();
			List<List<Integer>> ballots = new ArrayList<List<Integer>>();
			HashMap<Integer, List<Integer>> topCandidatesIndices = new HashMap<Integer, List<Integer>>();
			List<String> candidateNames = new ArrayList<String>();
			candidateNames.add("");
			int totalCandidates = Integer.valueOf(TextReader.readNextLine());
			
			for(int candidate = 1; candidate <= totalCandidates; candidate++){
				String candidateName = TextReader.readNextLine();
				candidateNames.add(candidateName);
			}
			
			String ballotPreprocessed;
			int totalBallots = 0;
			while ( (ballotPreprocessed = TextReader.readNextLine()) != null){
				List<Integer> ballot = 
					CommonUtils.intArrayToList(
						CommonUtils.stringArrayToIntegerArray(ballotPreprocessed.split(" ")));
				List<Integer> topIndices = topCandidatesIndices.get(ballot.get(0));
				if(topIndices == null) topIndices = new ArrayList<Integer>();
				topIndices.add(ballots.size());
				ballots.add(ballot);
				topCandidatesIndices.put(ballot.get(0), topIndices);
				totalBallots++;
			}

			boolean found = false;
			while(!found){
				int min = topCandidatesIndices.get(1).size();
				List<Integer> minCandidates = new ArrayList<Integer>();
				
				for(int candidate = 1; candidate <= totalCandidates; candidate++){
					BigDecimal percent = CommonUtils.ratio(topCandidatesIndices.get(candidate).size(), totalBallots);
					percentVotes.put(candidate, percent);
					if(percent.compareTo(new BigDecimal(0.5)) == 1){
						System.out.println(candidateNames.get(candidate));
						found = true;
						break;
					}
					if(topCandidatesIndices.get(candidate).size() < min) min = topCandidatesIndices.get(candidate).size();
				}
				if(found) continue;
				for(int candidate = 1; candidate <= totalCandidates; candidate++){
					if(topCandidatesIndices.get(candidate).size() == min) minCandidates.add(candidate);
				}
				
				for(Integer candidate : minCandidates){
					List<Integer> topCandidateIndices = topCandidatesIndices.get(candidate);
					for(Integer index : topCandidateIndices){
						List<Integer> ballot = ballots.get(index);
						ballot.remove(0);
						List<Integer> nextTopCandidateIndices = topCandidatesIndices.get(ballot.get(0));
						nextTopCandidateIndices.add(index);
					}
					for(int index = 0; index < topCandidateIndices.size(); index++) topCandidateIndices.remove(index);
					topCandidatesIndices.remove(candidate);
					candidateNames.remove((int)candidate);
				}
			}
		}
	}
}
