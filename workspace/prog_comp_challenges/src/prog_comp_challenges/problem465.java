package prog_comp_challenges;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import sort.Quick;
import commonutils.CommonUtils;
import commonutils.TextReader;

public class problem465 {
	public static void main(String [] args){
		List<BigDecimal> ratioList = new ArrayList<BigDecimal>();
		TextReader.initialize("/media/beanz/Data/programming_competitions_practice/samples/110405.inp");
		int noOfCases = Integer.valueOf(TextReader.readNextLine());
		TextReader.readNextLine();
		for(int caseNo = 0; caseNo < noOfCases; caseNo++){
			int noOfJobs = Integer.valueOf(TextReader.readNextLine());
			for(int jobNo = 0; jobNo < noOfJobs; jobNo++){
				String timeAndPenalty = TextReader.readNextLine();
				String[] tokens = timeAndPenalty.split(" ");			
				ratioList.add(CommonUtils.ratio(tokens[1], tokens[0]));
			}
			TextReader.readNextLine();
			int length = ratioList.size();
			Quick.sort(ratioList, 0, length-1);
			
			HashMap<BigDecimal, List<Integer>> mapping = new HashMap<BigDecimal, List<Integer>>();
			for(int i = 0; i < ratioList.size(); i++){
				List<Integer> items = mapping.get(ratioList.get(i));
				if(items == null) items = new ArrayList<Integer>();
				items.add(i+1);
				mapping.put(ratioList.get(i), items);
			}
			for(int i = ratioList.size()-1; i >= 0;){
				List<Integer> items = mapping.get(ratioList.get(i));
				for(int j = 0; j < items.size(); j++){
					System.out.print(items.get(j) + " ");
					i--;
				}
			}
		}
		
	}
}
