package prog_comp_challenges;

import graphcontainers.AdjList;
import graphcontainers.Graph;
import graphcontainers.Vertex;
import graphtraversalcontainers.ShortestPathUtils;

import java.util.ArrayList;
import java.util.List;

import commonutils.TextReader;

public class problem1053 {
	public static void main(String [] args){
		TextReader.initialize("/media/beanz/Data/programming_competitions_practice/samples/111003.inp");
		int totalCases = Integer.valueOf(TextReader.readNextLine());
		TextReader.readNextLine();
		
		for(int caseNo = 0; caseNo < totalCases; caseNo++){
			String[] noOfFireStationsAndIntersections = TextReader.readNextLine().split(" ");
			int totalFireStations = Integer.valueOf(noOfFireStationsAndIntersections[0]);
			int totalIntersections = Integer.valueOf(noOfFireStationsAndIntersections[1]);
			
			List<Integer> fireStations = new ArrayList<Integer>();
			for(int station = 0; station < totalFireStations; station++)
				fireStations.add(Integer.valueOf(TextReader.readNextLine()));
			AdjList<Integer> copy = new AdjList<Integer>();
			AdjList<Integer> graph = new AdjList<Integer>();
			for(int intersection = 0; intersection < totalIntersections; intersection++){
				String[] sourceAndTargetAndCost = TextReader.readNextLine().split(" ");
				int source = Integer.valueOf(sourceAndTargetAndCost[0]);
				int target = Integer.valueOf(sourceAndTargetAndCost[1]);
				int cost = Integer.valueOf(sourceAndTargetAndCost[2]);
				graph.addEdge(source,target,cost);
				graph.addEdge(target,source,cost);
				copy.addEdge(source, target, cost);
				copy.addEdge(target,source,cost);
			}
			
			int max = 0;
			int fireStation = 0;
			
			for(Vertex<Integer> source : graph.vertexObjs()){
				updateFireStationLocations(source, graph, copy, fireStations);
				ShortestPathUtils.nonNegWeightedShortestPath(graph, source.getId());
				for(Vertex<Integer> vertex : graph.vertexObjs()){
					if(vertex.getData() > max){
						max = vertex.getData();
						fireStation = vertex.getId();
					}
				}
			}
			System.out.println(fireStation);
		}
	}
	
	public static void updateFireStationLocations(
		Vertex<Integer> source, Graph<Integer> graph, Graph<Integer> copy, List<Integer> fireStations){
		
		for(Vertex<Integer> vertex : graph.vertexObjs()){
			if(fireStations.contains(vertex.getId()))
					vertex.setData(0);
			else if(vertex.getId() == source.getId())
				vertex.setData(0);
			else
				vertex.setData(copy.vertex(vertex.getId()).getData());
		}
	}

}
