package prog_comp_challenges;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import commonutils.TextReader;
import containers.Stack;

public class problem467 {
	public static void main(String[] args){
		TextReader.initialize("/media/beanz/Data/programming_competitions_practice/samples/110407.inp");
		int noOfCases = Integer.valueOf(TextReader.readNextLine());
		for(int caseNo = 0; caseNo < noOfCases; caseNo++){
			int noOfTurtles = Integer.valueOf(TextReader.readNextLine());
			HashMap<String, Integer> indexMap = new HashMap<String, Integer>();
			List<String> unsortedTurtles = new ArrayList<String>();
			for(int turtle = 0; turtle < noOfTurtles; turtle++){
				String name = TextReader.readNextLine();
				indexMap.put(name, turtle);
				unsortedTurtles.add(name);
			}
			List<Integer> sortedTurtles = new ArrayList<Integer>();
			for(int turtle = 0; turtle < noOfTurtles; turtle++) sortedTurtles.add(indexMap.get(TextReader.readNextLine()));
			Stack<Integer> stack = new Stack<Integer>();
			for(int turtle = noOfTurtles-1; turtle >= 0; turtle--) stack.push(turtle);
			
			int prev = 0;
			for(int turtle = noOfTurtles-1; turtle >= 0; turtle--){
				int curr = sortedTurtles.get(turtle);
				if(curr < prev || turtle == noOfTurtles-1){
					prev = curr;
					continue;
				}
				while(turtle >= 0){
					System.out.println(unsortedTurtles.get(curr));
					stack.turtlePop(curr);
					turtle--;
					if(turtle < 0) break;
					curr = sortedTurtles.get(turtle);
				}
				break;
			}
			System.out.println();
		}
	}
}
