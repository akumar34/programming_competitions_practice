package prog_comp_challenges;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import sort.Pigeonhole;
import commonutils.TextReader;
import containers.Pair;
import containers.Stack;

public class problem864 {
	public static void main(String [] args){
		HashMap<Integer, Integer> totalRoutes = new HashMap<Integer, Integer>();
		HashMap<Integer, List<Integer>> neighbors = new HashMap<Integer, List<Integer>>();
		TextReader.initialize("/media/beanz/Data/programming_competitions_practice/samples/110804.inp");
		
		String[] townsAndRoutesCount = TextReader.readNextLine().split(" ");
		int noOfTowns = Integer.valueOf(townsAndRoutesCount[0]);
		int noOfRoutes = Integer.valueOf(townsAndRoutesCount[1]);
		int[][] graph = new int[noOfTowns+1][noOfTowns+1];
		for(int i = 0; i < noOfTowns + 1; i++)
			for(int j = 0; j < noOfTowns + 1; j++)
				graph[i][j] = 0;
		
		for(int routeNo = 0; routeNo < noOfRoutes; routeNo++){
			String[] sourceAndDestinationTown = TextReader.readNextLine().split(" ");
			int source = Integer.valueOf(sourceAndDestinationTown[0]);
			int destination = Integer.valueOf(sourceAndDestinationTown[1]);
			graph[source][destination] = 1;
			graph[destination][source] = 1;
			updateTotalRoutes(neighbors, source, destination, totalRoutes);
			updateTotalRoutes(neighbors, destination, source, totalRoutes);
		}
		
		List<Pair> totalRoutesByStation = new ArrayList<Pair>(noOfTowns);
		for(int i = 0; i < noOfTowns + 1; i++) totalRoutesByStation.add(new Pair(0,0));
		
		for(int station : totalRoutes.keySet()){
			int totalStationRoutes = totalRoutes.get(station);
			totalRoutesByStation.set(station, new Pair(station, totalStationRoutes));
		}
		
		Pigeonhole.sort(totalRoutesByStation);
		totalRoutesByStation.remove(0);
		
		Collections.reverse(totalRoutesByStation);
		Stack<Integer> uncovered = new Stack<Integer>();
		for(Pair point : totalRoutesByStation) 
			uncovered.push((int)point.getFirst());
		int min = stations(neighbors, uncovered, new ArrayList<Integer>(), new Stack<Integer>(uncovered), 1000);
		System.out.println(min);
	}
	
	public static int stations(
			HashMap<Integer, List<Integer>> neighbors, 
			Stack<Integer> uncovered, 
			List<Integer> selected, 
			Stack<Integer> originalUncovered,
			int min){
		if(uncovered.isEmpty()){
			if(selected.size() < min) min = selected.size();
			return selected.size();
		}
		
		for(int source : originalUncovered){
			Stack<Integer> oldUncovered = new Stack<Integer>(uncovered);
			for(int neighbor : neighbors.get(source)) uncovered.remove(neighbor);

			uncovered.remove(source);
			originalUncovered.remove(source);
			selected.add(source);
			
			if(contains(neighbors.get(source), oldUncovered) && selected.size() < min){
				int count = stations(neighbors, uncovered, selected, originalUncovered, min);
				if(count < min) min = count;
				break;
			}
		}
		return min;
	}
	
	public static boolean contains(List<Integer> list, Stack<Integer> stack){
		for(int item : list) if(stack.contains(item)) return true;
		return false;
	}
	
	public static void updateTotalRoutes(
			HashMap<Integer,List<Integer>> neighbors, 
			int source, 
			int destination, 
			HashMap<Integer,Integer> totalRoutes){
		List<Integer> neighborsList = neighbors.get(source);
		if(neighborsList == null) neighborsList = new ArrayList<Integer>(); 
		neighborsList.add(destination);
		neighbors.put(source,neighborsList);
		Integer count = totalRoutes.get(source);
		if(count == null) count = new Integer(0);
		count++;
		totalRoutes.put(source,count);
	}
}
