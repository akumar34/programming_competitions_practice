package prog_comp_challenges;

import graphcontainers.AdjList;
import graphcontainers.GraphEdge;
import graphcontainers.Vertex;
import graphtraversalcontainers.GraphTraversalUtils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import commonutils.TextReader;

public class problem965 {
	@SuppressWarnings("unchecked")
	public static void main(String [] args){
		TextReader.initialize("/media/beanz/Data/programming_competitions_practice/samples/110905.inp");
		String word = null;
		List<String> vertices = new ArrayList<String>();
		vertices.add(null);
		while((word = TextReader.readNextLine()) != null) vertices.add(word);
		
		AdjList<Integer> graph = new AdjList<Integer>();
		for(int i = 1; i < vertices.size(); i++){
			for(int j = i + 1; j < vertices.size(); j++){
				if(StringUtils.getLevenshteinDistance(vertices.get(i), vertices.get(j)) == 1){
					graph.addEdge(i, j, 1);
				}
			}
		}
		int max = 0;
		List<Integer> sorted = GraphTraversalUtils.topologicalSort(graph);
		List<Vertex<Integer>> vertexObjs = new ArrayList<Vertex<Integer>>();
		for(int vertex : sorted){
			Vertex<Integer> vertexObj = graph.vertex(vertex);
			if(vertexObj.getInDegree() == 0 && vertexObj.getOutDegree() == 0) continue;
			vertexObj.setData(Integer.MIN_VALUE);
			vertexObjs.add(vertexObj);
		}
		vertexObjs.get(0).setData(0);
		
		for(Vertex<Integer> vertex : vertexObjs){
			for(Vertex<Integer> neighbor : graph.neighbors(vertex)){
				GraphEdge<Integer> edge = (GraphEdge<Integer>) graph.edge(vertex, neighbor);
				if(vertex.getData() + edge.getCost() > neighbor.getData()){
					neighbor.setData(vertex.getData() + edge.getCost());
					if(neighbor.getData() > max) max = neighbor.getData();
				}
			}
		}
		
		System.out.println(max+1);
		
	}
}
