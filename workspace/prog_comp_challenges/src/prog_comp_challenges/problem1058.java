package prog_comp_challenges;

import java.util.ArrayList;
import java.util.List;

import networkflowcontainers.NetworkEdge;
import networkflowcontainers.NetworkFlowUtils;
import networkflowcontainers.NetworkGraph;
import commonutils.TextReader;

public class problem1058 {
	@SuppressWarnings("unchecked")
	public static void main(String [] args){
		TextReader.initialize("/media/beanz/Data/programming_competitions_practice/samples/111008.inp");
		String input = null;
		while((input = TextReader.readNextLine()) != null){
			String[] totalCategriesAndProblems = input.split(" ");
			int totalCategories = Integer.valueOf(totalCategriesAndProblems[0]);
			int totalProblems = Integer.valueOf(totalCategriesAndProblems[1]);
			if(totalCategories == 0 && totalProblems == 0) break;
			String[] categoryCapacityTokens = TextReader.readNextLine().split(" ");
			
			NetworkGraph<Integer> graph = new NetworkGraph<Integer>();
			int source = 1;
			int sink = source + totalCategories + 1 + totalProblems;
			List<Integer> categoryIndices = new ArrayList<Integer>();
			categoryIndices.add(null);
			for(int i = 0; i < totalCategories; i++){
				int capacity = Integer.valueOf(categoryCapacityTokens[i]);
				graph.addEdge(source, i+source+1, capacity, 0);
				categoryIndices.add(i+source+1);
			}
			
			for(int probNo = 0; probNo < totalProblems; probNo++){
				String[] problem = TextReader.readNextLine().split(" ");
				
				for(int parameterNo = 1; parameterNo <= Integer.valueOf(problem[0]); parameterNo++){
					int category = categoryIndices.get(Integer.valueOf(problem[parameterNo]));
					int vertex = source + totalCategories + 1 + probNo;
					graph.addEdge(category, vertex,1,0);
				}
			}
			
			for(int probNo = 0; probNo < totalProblems; probNo++){
				int vertex = source + totalCategories + 1 + probNo;
				graph.addEdge(vertex,sink,1,0);
			}
			
			graph = NetworkFlowUtils.maximumFlow(graph, source, sink);
			
			for(int catIndex = 1; catIndex <= totalCategories; catIndex++){
				int category = categoryIndices.get(catIndex);
				for(int neighbor : graph.neighbors(category)){
					NetworkEdge<Integer> edge = (NetworkEdge<Integer>)graph.edge(category, neighbor);
					if(edge.getFlow() > 0) System.out.print((neighbor-source-totalCategories) + " ");
				}
				System.out.println();
			}
		}
	}
}
