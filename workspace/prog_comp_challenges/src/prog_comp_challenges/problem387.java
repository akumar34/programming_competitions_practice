package prog_comp_challenges;

import graphcontainers.AdjList;
import graphtraversalcontainers.DepthFirst;
import graphtraversalcontainers.GraphTraversal;
import graphtraversalcontainers.GraphTraversalUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import commonutils.TextReader;

public class problem387 {
	public static void main(String [] args){
		TextReader.initialize("/media/beanz/Data/programming_competitions_practice/samples/110307.inp");
		List<String> dictionary = new ArrayList<String>();
		dictionary.add(null);
		
		HashMap<String, Integer> wordMapping = new HashMap<String, Integer>();
		HashMap<Integer, String> indexMapping = new HashMap<Integer, String>();
		String word = "";
		while((word = TextReader.readNextLine()) != null){
			if(word.equals("")) break;
			dictionary.add(word);
			wordMapping.put(word, dictionary.size()-1);
			indexMapping.put(dictionary.size()-1, word);
		} 
		
		AdjList<Integer> graph = new AdjList<Integer>();
		for(int i = 1; i < dictionary.size(); i++){
			for(int j = i + 1; j < dictionary.size(); j++){
				if(editDistance(dictionary.get(i), dictionary.get(j)) == 1)
					graph.addEdge(i,j,1);
			}
		}
		
		while((word = TextReader.readNextLine()) != null){
			String[] startAndEnd = word.split(" ");
			String start = startAndEnd[0];
			String end = startAndEnd[1];
		
			GraphTraversal<Integer> dfs = new DepthFirst<Integer>(graph);
			dfs.search(wordMapping.get(start));
			List<Integer> path = GraphTraversalUtils.path(wordMapping.get(start), wordMapping.get(end), dfs.pathMap());
			
			if(path == null){;
				System.out.println("No solution.");
				return;
			}
			
			for(int vertex : path) System.out.println(indexMapping.get(vertex));
			
			System.out.println();
		}
	}

	public static int editDistance(String source, String target){
		int distance = 0;
		if(source.length() != target.length()) return -1;
		char[] sourceArray = source.toCharArray();
		char[] targetArray = target.toCharArray();
		for(int i = 0; i < source.length(); i++) 
			if(sourceArray[i] != targetArray[i]) distance++;
		return distance;
	}
}
