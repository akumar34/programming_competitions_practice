package prog_comp_challenges;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import commonutils.CommonUtils;
import commonutils.TextReader;
import containers.Pair;

public class problem865 {
	public static void main(String [] args){
		TextReader.initialize("/media/beanz/Data/programming_competitions_practice/samples/110805.inp");
		int noOfCases = Integer.valueOf(TextReader.readNextLine());
		TextReader.readNextLine();
		List<Integer> items = new ArrayList<Integer>();
		String item = null;
		int noOfPeople = Integer.valueOf(TextReader.readNextLine());
		for(int caseNo = 0; caseNo < noOfCases; caseNo++){
			while( (item = TextReader.readNextLine()) != null) items.add(Integer.valueOf(item));
			Collections.reverse(items);
			Collections.sort(items);
			Integer[] arrayObject = (Integer[])items.toArray(new Integer[noOfPeople]);
			int[] array = new int[noOfPeople];
			for(int i = 0; i < array.length; i++) array[i] = arrayObject[i];

			Pair<int[],int[]> result = tugOfWar(array, 0, 0,0,new int[noOfPeople], new int[noOfPeople], 0, 0,new int[noOfPeople], new int[noOfPeople]);
		
			Pair<int[],int[]>  A_B = (Pair<int[],int[]> )result;
			int A[] = (int[])A_B.getFirst();
			int B[] = (int[])A_B.getSecond();
			
			System.out.println(total(A) + " " + total(B));
		}
	}
	
	public static Pair<int[],int[]> tugOfWar(
		int[] array,
		int i,
		int j_A,
		int j_B,
		int[] A,
		int[] B,
		int W_A,
		int W_B,
		int[] A_min,
		int[] B_min
	){
		if(i == array.length){
			int W_A_min = total(A_min);
			int W_B_min = total(B_min);
			if(W_A_min == W_B_min && W_A_min == 0) W_A_min = 1000;
			int min = CommonUtils.absoluteDifference(W_A_min, W_B_min);
			if( CommonUtils.absoluteDifference(W_A, W_B) < min){
				int cnt_A = 0;
				int cnt_B = 0;
				for(int j = 0; j < array.length; j++) if(A[j] != 0) cnt_A++;
				for(int j = 0; j < array.length; j++) if(B[j] != 0) cnt_B++;
				
				if(cnt_A == cnt_B || cnt_A == cnt_B + 1 || cnt_A == cnt_B - 1){
					for(int j = 0; j < array.length; j++) A_min[j] = A[j];
					for(int j = 0; j < array.length; j++) B_min[j] = B[j];		
				}
			}
			return new Pair<int[],int[]>(A_min,B_min);
		}
		int weight = array[i];
		A[j_A] = weight;
		
		tugOfWar(array, i + 1, j_A + 1, j_B, A, B, W_A + weight, W_B, A_min, B_min);
		for(int j = j_A; j < array.length; j++) A[j] = 0;

		B[j_B] = weight;
		tugOfWar(array, i + 1, j_A, j_B + 1, A, B, W_A, W_B + weight, A_min, B_min);
		for(int j = j_B; j < array.length; j++) B[j] = 0;
		
		return new Pair<int[],int[]>(A_min,B_min);
	}
	public static int total(int[] list){
		int sum = 0;
		for(int item : list) sum += item;
		return sum;
	}
}
