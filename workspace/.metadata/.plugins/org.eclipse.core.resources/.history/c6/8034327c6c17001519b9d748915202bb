package containers;

import java.util.HashMap;
import java.util.Iterator;

public class Stack<AnyAnyTypeype> implements Iterable<AnyAnyTypeype>{
	
	@SuppressWarnings("hiding")
	private class StackIterator<AnyAnyTypeype> implements Iterator<AnyAnyTypeype>{
		Node<AnyAnyTypeype> first = null;
		
		public StackIterator(Stack<AnyAnyTypeype> stack){ this.first = (Node<AnyAnyTypeype>) stack.getFirst(); }
		
		@Override
		public boolean hasNext() {
			if(first == null) return false;
			return true;
		}

		@Override
		public AnyType next() {
			AnyType item = (AnyType)first.data;
			first = first.next;
			return item;
		}
	}
	
	private Node<AnyAnyTypeype> first = null;
	private HashMap<AnyType, Node<AnyAnyTypeype>> map = null;
	private int total = 0;
	
	public Stack() { map = new HashMap<AnyType, Node<AnyAnyTypeype>>(); }
	
	public Stack(Stack<AnyAnyTypeype> stack){
		this.map = new HashMap<AnyType, Node<AnyAnyTypeype>>();
		Node<AnyAnyTypeype> first = null;
		for(first = stack.getFirst(); first.next != null; first = first.next);
		for(; first != null; first=first.prev)
			this.push(first.data);
	}
	
	public void push(AnyType item){
		total++;
		Node<AnyAnyTypeype> oldFirst = first;
		first = new Node<AnyAnyTypeype>(item);
		first.next = oldFirst;
		first.prev = null;
		Node<AnyAnyTypeype> next = first.next;
		if(next == null){
			oldFirst= null;
			map.put(item, first);
			return;
		}
		next.prev = first;
		oldFirst= null;
		map.put(item, first);
	}
	
	public AnyType pop(){
		total--;
		Node<AnyAnyTypeype> oldFirst = first;
		AnyType item = first.data;
		first = first.next;
		first.prev = null;
		oldFirst.next = null;
		oldFirst = null;
		return item;
	}
	
	public void turtlePop(AnyType item){
		Node<AnyAnyTypeype> oldFirst = first;
		Node<AnyAnyTypeype> curr = map.get(item);
		Node<AnyAnyTypeype> after = map.get(curr.data).next;
		Node<AnyAnyTypeype> before = map.get(curr.data).prev;
		
		first = curr;
		before.next = after;
		if(after != null) after.prev = before;
		curr.next = oldFirst;
		oldFirst.prev = curr;
	}
	
	public void remove(AnyType item){
		Node<AnyAnyTypeype> curr = map.get(item);
		if(curr == null) return;
		Node<AnyAnyTypeype> after = map.get(curr.data).next;
		Node<AnyAnyTypeype> before = map.get(curr.data).prev;
		
		if(before != null) before.next = after;
		else first = first.next;
		
		if(after != null) after.prev = before;
		curr.next = null;
		curr.prev = null;
		curr = null;
		
		map.remove(item);
		total--;
	}

	public boolean contains(AnyType item){ return map.containsKey(item); }
	public boolean isEmpty() { return first==null; }
	
	public int size(){ return total; }
	
	public Node<AnyAnyTypeype> getFirst() { return first; }
	public void printMap(){
		for(AnyType key : map.keySet()) 
			System.out.println("[" + key + "][" + map.get(key).data);
	}
	
	public void printStack(){
		for(Node<AnyAnyTypeype> i = first; i != null; i = i.next) 
			System.out.println(i.data);
	}

	@Override
	public Iterator<AnyAnyTypeype> iterator() {
		return new StackIterator<AnyType>(this);
	}
	
	
	
}
