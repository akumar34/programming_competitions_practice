package graphtraversal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import containers.Graph;
import containers.Queue;

public class BreadthFirst<AnyType> {
	public static <AnyType> HashMap<AnyType, AnyType> search(Graph<AnyType> graph, AnyType source){
		HashMap<AnyType, Boolean> processed = new HashMap<AnyType, Boolean>();
		HashMap<AnyType, Boolean> discovered = new HashMap<AnyType, Boolean>();
		HashMap<AnyType, AnyType> pathMap = new HashMap<AnyType, AnyType>();
		
		for(AnyType vertex : graph.vertices()){
			processed.put(vertex, false);
			discovered.put(vertex, false);
			pathMap.put(vertex, null);
		}
		search(graph,source,processed,discovered,pathMap);
	}
	public static <AnyType> HashMap<AnyType, AnyType> search(
			Graph<AnyType> graph, 
			AnyType source,
			HashMap<AnyType, Boolean> processed,
			HashMap<AnyType, Boolean> discovered,
			HashMap<AnyType, AnyType> pathMap
			){
		
		Queue<AnyType> queue = new Queue<AnyType>();
		queue.push(source);
		discovered.put(source, Boolean.TRUE);
		while(! queue.isEmpty()){
			AnyType vertex = queue.pop();
			processVertex(vertex);
			processed.put(vertex, Boolean.TRUE);
			List<AnyType> neighbors = graph.neighbors(vertex);
			for(AnyType neighbor : neighbors){
				if(discovered.get(neighbor) == Boolean.FALSE){
					queue.push(neighbor);
					discovered.put(neighbor, Boolean.TRUE);
					pathMap.put(neighbor, vertex);
				}
				if(processed.get(neighbor) == Boolean.FALSE)
					processEdge(vertex, neighbor);
			}
		}
		return pathMap;
	}
	
	public static <AnyType> List<AnyType> path(HashMap<AnyType, AnyType> pathMap, AnyType start, AnyType end){
		return recursePath(pathMap, start, end, new ArrayList<AnyType>());
	}
	private static <AnyType> List<AnyType> recursePath(HashMap<AnyType, AnyType> pathMap, AnyType start, AnyType end, List<AnyType> path){
		if(start.equals(end) || end == null)
			path.add(start);
		else{
			recursePath(pathMap, start, pathMap.get(end), path);
			path.add(end);
		}
		return path;
	}
	public static <AnyType> void processVertex(AnyType vertex){
		//System.out.println("processed vertex " + vertex);
	}
	public static <AnyType> void processEdge(AnyType source, AnyType target){
		//System.out.println("processed edge (" + source + "," + target + ")");
	}
}
