package graphtraversal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import containers.Graph;

public class DepthFirst {
	private static boolean isCycle;
	
	public static <AnyType> HashMap<AnyType, AnyType> search(Graph<AnyType> graph, AnyType source){
		HashMap<AnyType, Boolean> processed = new HashMap<AnyType, Boolean>();
		HashMap<AnyType, Boolean> discovered = new HashMap<AnyType, Boolean>();
		HashMap<AnyType, AnyType> pathMap = new HashMap<AnyType, AnyType>();
		
		for(AnyType vertex : graph.vertices()){
			processed.put(vertex, false);
			discovered.put(vertex, false);
			pathMap.put(vertex, null);
		}
		
		isCycle = false;
		return search(graph,source,processed,discovered,pathMap);
	}
	public static <AnyType> HashMap<AnyType, AnyType> search(Graph<AnyType> graph, AnyType source, HashMap<AnyType, Boolean> discovered){
		HashMap<AnyType, Boolean> processed = new HashMap<AnyType, Boolean>();
		discovered = new HashMap<AnyType, Boolean>();
		HashMap<AnyType, AnyType> pathMap = new HashMap<AnyType, AnyType>();
		
		for(AnyType vertex : graph.vertices()){
			processed.put(vertex, false);
			discovered.put(vertex, false);
			pathMap.put(vertex, null);
		}
		
		isCycle = false;
		return search(graph,source,processed,discovered,pathMap);
	}
	public static <AnyType> HashMap<AnyType, AnyType> search(
			Graph<AnyType> graph, 
			AnyType source,
			HashMap<AnyType, Boolean> processed,
			HashMap<AnyType, Boolean> discovered,
			HashMap<AnyType, AnyType> pathMap
			){
		
		discovered.put(source, Boolean.TRUE);
		
		for(AnyType neighbor : graph.neighbors(source)){
			if(discovered.get(neighbor) == Boolean.FALSE){
				pathMap.put(neighbor, source);
				search(graph, neighbor, processed, discovered, pathMap);
			}
			else
				if(processed.get(neighbor) == Boolean.FALSE)
					processEdge(source, neighbor, pathMap);
		}
		processed.put(source, Boolean.TRUE);
		return pathMap;
	}
	
	public static <AnyType> List<AnyType> path(HashMap<AnyType, AnyType> pathMap, AnyType start, AnyType end){
		return recursePath(pathMap, start, end, new ArrayList<AnyType>());
	}
	private static <AnyType> List<AnyType> recursePath(HashMap<AnyType, AnyType> pathMap, AnyType start, AnyType end, List<AnyType> path){
		if(start.equals(end) || end == null)
			path.add(start);
		else{
			recursePath(pathMap, start, pathMap.get(end), path);
			path.add(end);
		}
		return path;
	}
	public static boolean isCycle(){
		return isCycle;
	}
	public static <AnyType> boolean isCycle(Graph<AnyType> graph, AnyType source){
		search(graph, source);
		return isCycle;
	}
	public static <AnyType> void processVertex(AnyType vertex){
		//System.out.println("processed vertex " + vertex);
	}
	public static <AnyType> void processEdge(AnyType source, AnyType target, HashMap<AnyType,AnyType> pathMap){
		if(pathMap.get(source) != target) isCycle = true;
	}
}
