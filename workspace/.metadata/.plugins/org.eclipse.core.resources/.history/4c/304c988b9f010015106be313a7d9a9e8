package prog_comp_challenges;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import commonutils.TextReader;

public class problem284 {
	
	@SuppressWarnings("serial")
	public static HashMap<Character, Character> decryptionMapping = new HashMap<Character, Character>(){
	{
        put('a','*');put('b','*');put('c','*');put('d','*');put('e','*');put('f','*');put('g','*');
        put('h','*');put('i','*');put('j','*');put('k','*');put('l','*');put('m','*');put('n','*');
        put('o','*');put('p','*');put('q','*');put('r','*');put('s','*');put('t','*');put('u','*');
        put('v','*');put('w','*');put('x','*');put('y','*');put('z','*');put(' ',' ');
    }};
    
	public static void main(String [] args){
		TextReader.initialize("/media/beanz/Data/programming_competition_practice/samples/110204.inp");
		int totalDictionaryWords = Integer.valueOf(TextReader.readNextLine());
		HashMap<Integer, List<String>> dictionary = new HashMap<Integer, List<String>>();
		HashMap<Integer, List<String>> encryptionWords = new HashMap<Integer, List<String>>();
		HashMap<Integer,List<Boolean[]>> isDecryptedWordsMapping = new HashMap<Integer, List<Boolean[]>>();
		
		for(int curr = 0; curr < totalDictionaryWords; curr++){
			String word = TextReader.readNextLine();
			List<String> words = dictionary.get(word.length());
			if(words == null) words = new ArrayList<String>();
			if(! words.contains(word)) words.add(word);
			dictionary.put(word.length(), words);
		}
		String encryption = TextReader.readNextLine();
		String tokens[] = encryption.split(" ");
		for(String word : tokens){
			List<String> words = encryptionWords.get(word.length());
			List<Boolean[]> isDecryptedWords = isDecryptedWordsMapping.get(word.length());
			if(words == null) words = new ArrayList<String>();
			if(! words.contains(word)){
				words.add(word);
				if(isDecryptedWords == null) isDecryptedWords = new ArrayList<Boolean[]>();
				Boolean[] isDecryptedWord = new Boolean[word.length()];
				for(int index = 0; index < isDecryptedWord.length; index++) isDecryptedWord[index] = false;
				isDecryptedWords.add(isDecryptedWord);
			}
			encryptionWords.put(word.length(), words);
			isDecryptedWordsMapping.put(word.length(),isDecryptedWords);
		}
		if(! lengthMismatch(dictionary, encryptionWords)){
			System.out.println(encryption);
			System.out.println(buildOutput(encryption));
			return;
		}
		
		for(Integer wordLength : encryptionWords.keySet()){
			List<String> list1 = dictionary.get(wordLength);
			List<String> list2 = encryptionWords.get(wordLength);
			if(list1.size() != 1) continue;
			if(! updateDecryption(list1.get(0), list2.get(0)) ){
				System.out.println(encryption);
				System.out.println(buildOutput(encryption));
				return;
			}
		}
		
		for(Integer wordLength : encryptionWords.keySet()){
			List<String> words = encryptionWords.get(wordLength);
			for(int wordIndex = 0; wordIndex < words.size(); wordIndex++){
				String word = words.get(wordIndex);
				char[] wordChars = word.toCharArray();
				for(int charIndex = 0; charIndex < wordChars.length; charIndex++){
					char decryptedChar = decryptionMapping.get(wordChars[charIndex]);
					if(decryptedChar == '*') continue;
					wordChars[charIndex] = decryptedChar;
					words.set(wordIndex, new String(wordChars));
					encryptionWords.put(wordLength,words);
					
					Boolean[] isDecryptedWord = isDecryptedWordsMapping.get(wordLength).get(wordIndex);
					isDecryptedWord[charIndex] = true;
				}
			}
		}
		
		
		HashMap<Integer, HashMap<Integer, HashMap<Character, HashSet<String>>>> 
			wordFeatures = new HashMap<Integer, HashMap<Integer,HashMap<Character,HashSet<String>>>>();
		
		for(Integer wordLength : dictionary.keySet()){
			HashMap<Integer, HashMap<Character, HashSet<String>>> positionFeatures = wordFeatures.get(wordLength);
			if(positionFeatures == null) positionFeatures = new HashMap<Integer, HashMap<Character, HashSet<String>>>();
			for(String word : dictionary.get(wordLength)){
				char[] charArray = word.toCharArray();
				for(int position = 0; position < wordLength; position++){
					HashMap<Character, HashSet<String>> characterFeatures = positionFeatures.get(position);
					if(characterFeatures == null) characterFeatures = new HashMap<Character, HashSet<String>>();
					HashSet<String> features = characterFeatures.get(charArray[position]);
					if(features == null) features = new HashSet<String>();
					features.add(word);
					characterFeatures.put(charArray[position], features);
					positionFeatures.put(position, characterFeatures);
					wordFeatures.put(wordLength, positionFeatures);
				}				
			}
		}
		
		for(Integer wordLength: dictionary.keySet()){
			List<String> words = encryptionWords.get(wordLength);
			List<Boolean[]> isDecryptedWords = isDecryptedWordsMapping.get(wordLength);
			for(int wordIndex = 0; wordIndex < words.size(); wordIndex++){
				char[] charArray = words.get(wordIndex).toCharArray();
				Boolean[] isDecryptedWord = isDecryptedWords.get(wordIndex);
				List<HashSet<String>> wordsSet = new ArrayList<HashSet<String>>();
				for(int charIndex = 0; charIndex < wordLength; charIndex++){
					if(! isDecryptedWord[charIndex]) continue;
					wordsSet.add(wordFeatures.get(wordLength).get(charIndex).get(charArray[charIndex]));
					isDecryptedWord[charIndex] = true;
				}
				if(wordsSet.isEmpty()) continue;
				HashSet<String> set = new HashSet<String>(wordsSet.get(0));
				for(HashSet<String> aSet : wordsSet) set.retainAll(aSet);
				if(set.isEmpty()) continue;
				if(set.size() > 1) continue;
				words.set(wordIndex, (String)set.toArray()[0]);
				encryptionWords.put(wordLength, words);
				
			}
		}
		
		System.out.println(encryption);
		System.out.println(buildOutput(encryption));
	}
	public static boolean updateDecryption(String decryptedWord, String encryptedWord){
		char[] encryptedChars = encryptedWord.toCharArray();
		char[] decryptedChars = decryptedWord.toCharArray();
		
		for(int index = 0; index < encryptedChars.length; index++){
			char ch = decryptionMapping.get(encryptedChars[index]);
			if(ch == '*') 
				decryptionMapping.put(encryptedChars[index], decryptedChars[index]);
			else if(ch != decryptedChars[index]) 
				return false;
		}
		return true;
	}
	public static boolean lengthMismatch(
		HashMap<Integer,List<String>> map1, HashMap<Integer,List<String>> map2){
		
		for(Integer key1 : map1.keySet()){
			List<String> list1 = map1.get(key1);
			List<String> list2 = map2.get(key1);
			if(list1.size() == list2.size()) continue;
			if(list1.size() < list2.size()) return false;
		}
		for(Integer key2 : map2.keySet()){
			List<String> list1 = map1.get(key2);
			List<String> list2 = map2.get(key2);
			if(list1 == null) return false;
			if(list2.size() > list1.size()) return false;
		}
		return true;
	}
	public static String buildOutput(String encryption){
		String output = "";
		for(char token : encryption.toCharArray()) output += decryptionMapping.get(token);
		return output;
	}
	public static int[][] computeCooccurrence(HashMap<Integer,List<String>> dictionary){
		int[][] cooccurrence = new int[26][26];
		for(List<String> list : dictionary.values()){
			for(String word : list){
				char[] charArray = word.toCharArray();
				for(int index1 = 0; index1 < charArray.length; index1++){
					char root = charArray[index1];
					for(int index2 = 0; index2 < charArray.length; index2++){
						char other = charArray[index2];
						cooccurrence[(int)root-97][(int)other-97]++;
					}
				}
			}
		}
		return cooccurrence;
	}
}
